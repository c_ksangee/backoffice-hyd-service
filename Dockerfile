FROM node:8.11

COPY src /usr/src/app/src
COPY public /usr/src/app/public
COPY .env /usr/src/app/.env
COPY package.json /usr/src/app/package.json

RUN chmod -R 777 /usr/src/app
WORKDIR /usr/src/app/
RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]