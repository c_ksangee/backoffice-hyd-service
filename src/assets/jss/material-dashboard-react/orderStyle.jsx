// ##############################
// // // Order Form styles
// #############################


const orderStyle = {
    marginTop: {
        marginTop: 50,        
    },
    centerAlign : {
        textAlign: "center"
    },
    leftAlign : {
        textAlign: "left"
    },
    rightAlign : {
        textAlign: "right"
    },
    table : {
        display: "table",
        width: "100%",
        marginTop: 5
    },
    tableRow : {
        display: "table-row"
    },
    tableHead : {
        fontWeight: 700
    },
    tableCell : {
        display: "table-cell",        
        width: "50%",
        verticalAlign: "top"
    },
    heading : {
        textTransform: "capitalize"
    },
    paddingTop : {
        paddingTop: 30
    },
    chip: {
        marginTop: 20,
        padding: 5,
        fontSize: '1.2em'
    }
};

export default orderStyle;
