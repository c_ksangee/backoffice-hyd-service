// ##############################
// // // policy Form styles
// #############################

const policyStyle = {
    marginTop: {
        marginTop: 50,        
    },
    centerAlign : {
        textAlign: "center"
    },
    leftAlign : {
        textAlign: "left"
    },
    rightAlign : {
        textAlign: "right"
    },
    table : {
        display: "table",
        width: "100%",
        marginTop: 5
    },
    tableRow : {
        display: "table-row"
    },
    tableHead : {
        fontWeight: 700
    },
    tableCell : {
        display: "table-cell",        
        width: "50%",
        verticalAlign: "top"
    },
    heading : {
        textTransform: "capitalize"
    },
    paddingTop : {
        paddingTop: 30
    },
    chip: {
        padding: 5,
        marginTop: 10,
        fontSize: '1.2em'
    },
    error: {
        textAlign: 'center',
        color: 'red'
    },
    prioritySection : {
        border: '2px groove threedface',
        padding: '10px'
    },
    priority : {
        padding: '10px'
    },
    headSection : {
        padding:'5px',
        display :'flex'
    },
    leftNav : {
        float: 'left',
        width: '50%',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    rightNav : {
        float : 'right',
        width: '50%'
    },
    titleSection : {
        textAlign : 'left',
        fontSize : '17px',
        padding: '0 10px'
    },
    clear : {
        clear:'both'
    },
    hrClass :{
        color : '#666',
        margin:0
    },
    custTable:{
        padding:'10px'
    },
    custTableBody:{
        display:'block'
    },
    custTableRow : {
        width:'98%',
        overflow: 'hidden',
        outline: 'none',
        verticalAlign: 'middle',
        padding : '2px 10px'
    },
    custTableCell : {
        width : '50%',
        float : 'left',
        height: '48px',
        padding:'0 5px',
        boxSizing:'border-box'
    },
    pageSectionTitle : {
        fontSize : '15px'
    },
    editRuleContainer : {
        padding:'10px 25px'
    }

};

export default policyStyle;
