const rulesStyle = {

    conditionManager: {
        display: 'flex',
        border: '2px groove threedface'
    },    
    conditionSection: {
        display: 'flex',
        marginLeft: "10px"      
    },
    conditionChild: {
        padding: "10px",
        margin: "5px"
    },
    condition: {
        flex: 1,
        display: 'flex',
        padding: "10px 8px",
		cursor: "grabbing"
    },
    conditionSelectorWrap: {
        position: "relative",
        width: "102px"
    },
    conditionSelector: {
        position: "absolute",
        top: "49%",
        marginTop: "-5px",
        "&:after": {
            content: '""',
            width: "17px",
            position: "absolute",
            border: "1px solid #ccc",
            top: "11px",
            background: "black"
        }
    },
    actionIcon: {
        position: 'absolute',
        top: '-5px',
        right: '-5px'
    },
    groupOrRule: {
        position: "relative",
        "&:after, &:before": {
            content: '""',
            position: "absolute",
            left: "-10px",
            width: "20px",
            height: "calc(50% + 20px)",
            borderColor: "#cccccc",
            borderStyle: "solid",
            boxSizing: "border-box",
        },
        "&:before": {
            top: "-16px",
            borderWidth: "0 0 2px 2px",
        },
        "&:after": {
            top: "50%",
            borderWidth: "0 0 0 2px",
        }
    },
    groupOrRuleFirst: {
        position: "relative",
        "&:after": {
            content: '""',
            position: "absolute",
            left: "-10px",
            width: "20px",
            height: "calc(50% + 20px)",
            borderColor: "#cccccc",
            borderStyle: "solid",
            boxSizing: "border-box",
            top: "50%",
            borderWidth: "2px 0 0 2px",
        }
    },
    groupOrRuleLast: {
        position: "relative",
        "&:before": {
            content: '""',
            position: "absolute",
            left: "-10px",
            width: "20px",
            height: "calc(50% + 20px)",
            borderColor: "#cccccc",
            borderStyle: "solid",
            boxSizing: "border-box",
            top: "-16px",
            borderWidth: "0 0 2px 2px",
        }
    },
    op: {
        border: "1px solid grey",
        padding: "5px"
    },
    action: {
        border: '2px groove threedface',
        padding: '20px',
        margin: '30px 10px',
        width: '730px'
    }
}

export default rulesStyle;
