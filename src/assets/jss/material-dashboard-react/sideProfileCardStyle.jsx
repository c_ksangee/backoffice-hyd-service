// ##############################
// // // ProfileCard styles
// #############################

import { card, boxShadow, grayColor, defaultFont } from "assets/jss/material-dashboard-react.jsx";

const sideProfileCardStyle = {
  card: {
    marginTop: "0px",
    textAlign: "center",
    ...card
  },
  cardHeader: {
    display: "inline-block",
    width: "10%",
    padding: "0px",
    float:"left"
  },
  cardAvatar: {
    maxWidth: "130px",
    maxHeight: "130px",
    margin: "-45px -10px",
    borderRadius: "50%",
    overflow: "hidden",
    ...boxShadow
  },
  img: {
    width: "100%",
    height: "auto",
    verticalAlign: "middle",
    border: "0"
  },
  textAlign: {
    textAlign: "center"
  },
  cardSubtitle: {
    color: grayColor,
    ...defaultFont,
    fontSize: "1em",
    textTransform: "uppercase",
    marginTop: "10px",
    marginBottom: "10px"
  },
  cardTitle: {
    ...defaultFont,
    fontSize: "1.3em",
    marginTop: "10px",
    marginBottom: "10px"
  },
  cardDescription: {
    ...defaultFont,
    padding: "15px 20px",
    margin: "0 0 10px"
  },
  cardActions: {
    height: "auto",
    display: "inline"
  },
  cardContent:{
    padding:"0 ! important"
  },
  sideCard:{
    margin:"0 0 40px"
  }
};

export default sideProfileCardStyle;
