import React from 'react'
import BigCalendar from 'react-big-calendar'
import moment from 'moment';
import events from '../../variables/events'
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer
let allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])

let MyCalendar = () => (
  <BigCalendar
    events={events}
    views={allViews}
    step={60}
    showMultiDayTimes
  />
)

export default MyCalendar