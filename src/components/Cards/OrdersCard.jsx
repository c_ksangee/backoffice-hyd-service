import React from "react";
import PropTypes from "prop-types";
import {
  withStyles,
  Card,
  CardContent,
  CardHeader,
  Typography,
  Tabs,
  Tab
} from "@material-ui/core";
import { BugReport, Code, Cloud } from "@material-ui/icons";

import { Tasks,Table } from "components";

import { process } from "variables/general";

import tasksCardStyle from "assets/jss/material-dashboard-react/tasksCardStyle";

class OrdersCard extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.card}>
        <CardHeader
          classes={{
            root: classes.cardHeader,
            title: classes.cardTitle,
            content: classes.cardHeaderContent
          }}
          title="Orders Queue:"
          action={
            <Tabs
              classes={{
                flexContainer: classes.tabsContainer,
                indicator: classes.displayNone
              }}
              value={this.state.value}
              onChange={this.handleChange}
              textColor="inherit"
            >
              <Tab
                classes={{
                  wrapper: classes.tabWrapper,
                  labelIcon: classes.labelIcon,
                  label: classes.label,
                  textColorInheritSelected: classes.textColorInheritSelected
                }}
                icon={<BugReport className={classes.tabIcon} />}
                label={"Process"}
              />
              <Tab
                classes={{
                  wrapper: classes.tabWrapper,
                  labelIcon: classes.labelIcon,
                  label: classes.label,
                  textColorInheritSelected: classes.textColorInheritSelected
                }}
                icon={<Code className={classes.tabIcon} />}
                label={"Accepted"}
              />
              <Tab
                classes={{
                  wrapper: classes.tabWrapper,
                  labelIcon: classes.labelIcon,
                  label: classes.label,
                  textColorInheritSelected: classes.textColorInheritSelected
                }}
                icon={<Cloud className={classes.tabIcon} />}
                label={"Rejected"}
              />
            </Tabs>
          }
        />
        <CardContent>
          {this.state.value === 0 && (
            <Typography component="div">
              <Tasks
                checkedIndexes={[0]}
                tasksIndexes={[0, 1, 2, 3, 4, 5]}
                tasks={process}
              />
            </Typography>
          )}
          {this.state.value === 1 && (
            <Typography component="div">
              <Table
              tableHeaderColor="primary"
              tableHead={["Order No", "Item", "Name", "Price"]}
              tableData={[
                ["#ORD289347", "#R034I", "The Wondrous Nature™ Bouquet by FTD® - BASKET INCLUDED", "$36.738"],
                ["#ORD345345", "#HKE40", "In Full Bloom Peony Bouquet", "$23.789"],
                ["#ORD456664", "#TYI89", "Rush of Color Assorted Tulip Bouquet", "$56.142"],
                ["#ORD678686", "#B02", "The Precious Heart™ Bouquet by FTD® - VASE INCLUDED", "$38.735"],
                ["#ORD879789", "#Y893", "Naturally Pretty- VASE INCLUDED", "$63.542"],
                ["#ORD278997", "#SMRB", "The FTD® Best Day™ Bouquet- VASE INCLUDED", "$78.615"]
              ]}
            />
            </Typography>
          )}
          {this.state.value === 2 && (
            <Typography component="div">
              <Table
              tableHeaderColor="primary"
              tableHead={["Order No", "Item", "Name", "Price"]}
              tableData={[
                ["#ORD289347", "#T60P", "Come Rain or Come Shine Bouquet - VASE INCLUDED", "$35.738"],
                ["#ORD223447", "#HURI0", "Naturally Pretty- VASE INCLUDED", "$21.789"],
                ["#ORD223432", "#B02", "The Precious Heart™ Bouquet by FTD® - VASE INCLUDED", "$56.142"],
                ["#ORD282344", "#UIR9", "Lavender Fields Mixed Flower Bouquet - VASE INCLUDED", "$38.735"],
                ["#ORD245654", "#SMR0", "The FTD® Best Day™ Bouquet- VASE INCLUDED", "$53.542"],
                ["#ORD245646", "#JIO0", "Rush of Color Assorted Tulip Bouquet", "$38.615"]
              ]}
            />
            </Typography>
          )}
        </CardContent>
      </Card>
    );
  }
}

OrdersCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(tasksCardStyle)(OrdersCard);
