import React from "react";
import {
  withStyles,
  Card,
  CardHeader,
  CardContent,
  //CardActions,
  Typography
} from "@material-ui/core";
import PropTypes from "prop-types";

import sideProfileCardStyle from "assets/jss/material-dashboard-react/sideProfileCardStyle";

function SideProfileCard({ ...props }) {
//  const { classes, subtitle, title, description, footer, avatar } = props;
  const { classes, subtitle, title, description, avatar } = props;
  return (
    <Card className={classes.card +" "+ classes.sideCard}>
      <CardHeader
        classes={{
          root: classes.cardHeader,
          avatar: classes.cardAvatar
        }}
        avatar={<img src={avatar} alt="..." className={classes.img} />}
      />
      <CardContent className={classes.textAlign +" "+ classes.cardContent}>
        {subtitle !== undefined ? (
          <Typography component="h6" className={classes.cardSubtitle}>
            {subtitle}
          </Typography>
        ) : null}
        {title !== undefined ? (
          <Typography component="h4" className={classes.cardTitle}>
            {title}
          </Typography>
        ) : null}
        {description !== undefined ? (
          <Typography component="p" className={classes.cardDescription}>
            {description}
          </Typography>
        ) : null}
      </CardContent>
      {/* <CardActions className={classes.textAlign + " " + classes.cardActions}>
        {footer}
      </CardActions> */}
    </Card>
  );
}

SideProfileCard.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.node,
  subtitle: PropTypes.node,
  description: PropTypes.node,
  footer: PropTypes.node,
  avatar: PropTypes.string
};

export default withStyles(sideProfileCardStyle)(SideProfileCard);
