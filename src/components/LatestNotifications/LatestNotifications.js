import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import IconButton from '@material-ui/core/IconButton';
import UserList from "./UserList";
import CardMenu from "./CardMenu";


const CardLayout = ({children, styleName, childrenStyle}) => {
    return (
        <div className={`jr-card`}>
            {children}
        </div>
    )
};

function TabContainer({children, dir}) {
    return (
        <div dir={dir}>
            {children}
        </div>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

class LatestNotifications extends Component {

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleChangeIndex = index => {
        this.setState({value: index});
    };

    onOptionMenuSelect = event => {
        this.setState({menuState: true, anchorEl: event.currentTarget});
    };
    handleRequestClose = () => {
        this.setState({menuState: false});
    };

    constructor() {
        super();
        this.state = {
            value: 0,
            anchorEl: undefined,
            menuState: false,
        }
    }

    render() {
        const {anchorEl, menuState} = this.state;
        const {theme} = this.props;

        return (
            <CardLayout styleName="col-lg-12">
                <div className="jr-card-header mb-3 d-flex align-items-center">
                    <div className="mr-auto">
                        <h3 className="card-heading mb-0">
                        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 0h24v24H0z" fill="none"/>
                            <path d="M7.58 4.08L6.15 2.65C3.75 4.48 2.17 7.3 2.03 10.5h2c.15-2.65 1.51-4.97 3.55-6.42zm12.39 6.42h2c-.15-3.2-1.73-6.02-4.12-7.85l-1.42 1.43c2.02 1.45 3.39 3.77 3.54 6.42zM18 11c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2v-5zm-6 11c.14 0 .27-.01.4-.04.65-.14 1.18-.58 1.44-1.18.1-.24.15-.5.15-.78h-4c.01 1.1.9 2 2.01 2z"/>
                        </svg>
                            <div className="titleText">Latest Notifications</div>
                        </h3>
                    </div>
                    <IconButton className="size-30" onClick={this.onOptionMenuSelect.bind(this)}>
                        <i className="zmdi zmdi-more-vert"/>
                    </IconButton>
                </div>

                <div className="tab-notifications">
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered
                        fullWidth
                    >
                        <Tab className="tab" label="App Notifications"/>

                        <Tab className="tab" label="Announcements"/>
                    </Tabs>
                </div>

                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.value}
                    onChangeIndex={this.handleChangeIndex}
                >
                    <TabContainer dir={theme.direction}>
                        <UserList users={this.props.appNotification}/>
                    </TabContainer>
                    <TabContainer dir={theme.direction}>
                        <UserList users={this.props.announcementsNotification}/>
                    </TabContainer>

                </SwipeableViews>
                <CardMenu menuState={menuState} anchorEl={anchorEl}
                          handleRequestClose={this.handleRequestClose.bind(this)}/>
            </CardLayout>

        );
    }
}

LatestNotifications.propTypes = {
    theme: PropTypes.object.isRequired,
};

export default withStyles(null, {withTheme: true})(LatestNotifications);