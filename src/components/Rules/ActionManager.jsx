import React from 'react';
import { withStyles } from "@material-ui/core";
import rulesStyle from "assets/jss/material-dashboard-react/rulesStyle";

const ActionManager = ({ actionsList, onActionChange, readOnly, classes }) => {
    return (
        <div className={classes.conditionManager}>
            <div className={classes.conditionSection}>
                <p>Action to be implemented when above rule conditions meet.</p>
                <div className={classes.condition}>
                    <label className={classes.conditionChild}>Value : </label><input type="text" className={classes.conditionChild} disabled={readOnly} onChange={e => onActionChange({ value: e.target.value, valueType: actionsList.valueType })} value={actionsList.value} />
                    <label className={classes.conditionChild}>Value Type : </label><input type="text" className={classes.conditionChild} disabled={readOnly} onChange={e => onActionChange({ value: actionsList.value, valueType: e.target.value })} value={actionsList.valueType} />
                </div>
            </div>
        </div>
    );
}

export default withStyles(rulesStyle)(ActionManager);