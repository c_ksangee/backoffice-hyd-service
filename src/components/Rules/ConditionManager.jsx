import React from 'react';
import { withStyles } from '@material-ui/core';
import rulesStyle from "assets/jss/material-dashboard-react/rulesStyle";
import IconButton from '@material-ui/core/IconButton';
import { AddCircle, PlaylistAdd, RemoveCircle } from '@material-ui/icons';
import operators from "./Operators.json";
import { DragDropContext, DragSource, DropTarget } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import flow from 'lodash/flow';

export const ItemTypes = {
  CONDITION: 'condition'
};

const conditionsSource = {
  beginDrag(props) {
    const item = { id: props.id };
    return item;
  }
};

const conditionTarget = {
  drop(props, monitor, component) {
    
    props.onConditionMove(monitor.getItem().id, props.id);
    if (monitor.didDrop()) {
      // If you want, you can check whether some nested
      // target already handled drop
      return;
    }
  }
};

const collectDrag = (connect, monitor) => (
  {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
  }
)

const collectDrop = (connect, monitor) => (
  {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  }
)


export const Condition = flow(
  DragSource(ItemTypes.CONDITION, conditionsSource, collectDrag),
  DropTarget(ItemTypes.CONDITION, conditionTarget, collectDrop)
)(
  ({ id, fieldsList, classes, readOnly, condition: { entity, operator, value, field },
      onConditionRemove, onConditionMove, onFactChange, connectDragSource, connectDropTarget}) => {

    const innerCondition = <div className={classes.condition} key={id}>
        <div>
          <select onChange={e => onFactChange(id, { entity: e.target.value, field, operator, value })} value={entity} disabled={readOnly} className={classes.conditionChild} >
            <option value=""></option>
            {
              Object.keys(fieldsList).map((entityName) => {
                return <option key={`${id}${entityName}`} value={entityName}>{entityName}</option>
              })
            }
          </select>
        </div>
        <div>
          <select onChange={e => onFactChange(id, { entity, field: e.target.value, operator, value })} value={field} disabled={readOnly} className={classes.conditionChild} >
            <option value=""></option>
            {
              (typeof fieldsList[entity] !== 'undefined') &&
              Object.keys(fieldsList[entity]).map((attributeName) => {
                return <option key={`${id}${attributeName}`} value={attributeName}>{attributeName}</option>
              })
            }
          </select>
        </div>
        <div>
          <select onChange={e => onFactChange(id, { entity, field, operator: e.target.value, value })} disabled={readOnly} value={operator} className={classes.conditionChild} >
            <option value=""></option>
            {
              (typeof fieldsList[entity] !== 'undefined') && (typeof fieldsList[entity][field] !== 'undefined') &&
              operators[fieldsList[entity][field]].map((op) => {
                return <option key={`${id}${op.value}`} value={op.value}>{op.display}</option>
              })
            }
          </select>
        </div>
        <div>
          <input onChange={e => onFactChange(id, { entity, operator, value: e.target.value, field })} disabled={readOnly} className={classes.conditionChild} type='text' placeholder='value' value={value} />
        </div>
        {
          !readOnly &&
          <div>
            <IconButton title="Remove Condition" style={{ color: "red", width: "25px", height: "25px", top: "12px", left: "4px" }} onClick={e => { onConditionRemove(id); e.stopPropagation() }}>
              <RemoveCircle />
            </IconButton>
          </div>
        }
    </div>;

    return ( 
      readOnly ?  innerCondition : connectDragSource(connectDropTarget(innerCondition)) 
    )
  }
)

export const Conditions = ({ rulesList, op, top, id, classes, readOnly, ...rest }) => {

  const { onConditionAdd, onConditionAddGroup, onConditionRemoveGroup, onOpChange } = rest;

  return (
    <div className={classes.conditionSection}>
      <div className={classes.conditionSelectorWrap} style={{ width : readOnly ? "77px" : "" }}>
        <div className={classes.conditionSelector}>
          {
            !readOnly &&
            <IconButton title="Remove Condition Group" color="secondary" style={{ width: "25px", height: "25px",  visibility: top || readOnly ? "hidden" : "visible" }} onClick={e => { onConditionRemoveGroup(id); e.stopPropagation() }}>
              <RemoveCircle />
            </IconButton>
          }

          <select value={op} disabled={readOnly} onChange={e => { onOpChange(id.split('.').slice(0, -1).join('.'), e.target.value) }} style={{ height: 25 }}>
            <option value='all'>all</option>
            <option value='any'>any</option>
          </select>
        </div>
      </div>
      
      <div className="groupsOrRulescontainer">
        {
          rulesList.map((condition, i) => {
            const op = (typeof condition.all === 'object') ? 'all' : (typeof condition.any === 'object') ? 'any' : '';
            const groupOrRuleClassName = rulesList.length > 0
                ? (i === 0
                  ? (readOnly && rulesList.length === 1 ? '' : 'groupOrRuleFirst')
                  : ((readOnly && rulesList.length - 1 === i) ? 'groupOrRuleLast' : 'groupOrRule'))
                : '';
            
            return (
              <div className={classes[groupOrRuleClassName]} key={i}>
                {
                  (op !== '') ?
                    <Conditions id={`${id}[${i}].${op}`} op={op} classes={classes} rulesList={condition[op]} readOnly={readOnly} {...rest} /> //recursive
                    :
                    <Condition id={`${id}[${i}]`} classes={classes} condition={condition} readOnly={readOnly} {...rest} />
                }
              </div>
            )
          })
        }

        {
          !readOnly &&
          <div className={rulesList.length > 0 ? classes.groupOrRuleLast : ''}> 
            <IconButton style={{ color: "green" }} title="Add Condition" onClick={e => { onConditionAdd(id); e.stopPropagation() }}>
              <AddCircle />
            </IconButton>
            <IconButton color="primary" title="Add Condition Group" onClick={e => { onConditionAddGroup(id); e.stopPropagation() }}>
              <PlaylistAdd />
            </IconButton>
          </div>
        }
      </div>
    </div>
  )
}

export const ConditionManager = ({ rulesList, classes, ...rest }) => {

  const op = (typeof rulesList.all === 'object') ? 'all' : (typeof rulesList.any === 'object') ? 'any' : '';

  return (

    <div className={classes.conditionManager}>
      {
        op !== '' &&
        <Conditions id={`.${op}`} top op={op} classes={classes} rulesList={rulesList[op] || []} {...rest} />
      }
    </div>
  )
}

export default DragDropContext(HTML5Backend)(withStyles(rulesStyle)(ConditionManager));
