import React from "react";
import PaginationTable from 'components/Table/PaginationTable.jsx';
import rulesStyle from "assets/jss/material-dashboard-react/rulesStyle.jsx";

const Rules = ({ ...props }) => {
    const { classes, columnList, rulesList, onClick } = props;
    return (
        <PaginationTable
            columnData={columnList}
            bodyData={rulesList}
            onhandleClickCallback={onClick}
            classes={classes}
            keyColumn="ruleId"
            orderBy="ruleId"
            order="asc"
        />
    )
}



export default withStyles(rulesStyle)(Rules);