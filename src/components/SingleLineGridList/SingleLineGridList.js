import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';

function SingleLineGridList({ ...props }) {

    const tileData = [
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/B07D_LOL?&$ftd-tile-new$&wid=280',
            title: 'SMRB',
            author: 'jill111',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/FW20_LAY?&$ftd-tile-lay$&wid=280',
            title: 'FTKS0',
            author: 'director90',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/B05S_LOL?&$ftd-tile-new$&wid=280',
            title: 'IO54F',
            author: 'Danson67',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/FG27_LAY?&$ftd-tile-lay$&wid=280',
            title: 'GGT4',
            author: 'fancycrave1',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/B02D_LOL?&$ftd-tile-new$&wid=280',
            title: 'AAEG6',
            author: 'Hans',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/FW89_LAY?&$ftd-tile-lay$&wid=280',
            title: 'JDF5',
            author: 'fancycravel',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/BD2D_LOL?&$ftd-tile-new$&wid=280',
            title: 'HSDF4',
            author: 'jill111',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/FK1056_LAY?&$ftd-tile-lay$&wid=280',
            title: 'TTS3',
            author: 'BkrmadtyaKarki',
        },
        {
            img: '//s7d5.scene7.com/is/image/ProvideCommerce/18-S2D_LOL?&$ftd-tile-new$&wid=280',
            title: 'ERE5',
            author: 'PublicDomainPictures',
        }
    ];
    return (
        <div className="gl-single-line">
            <GridList className="slg" cols={7}>
                {tileData.map((tile, index) =>
                    <GridListTile key={index} classes="flowertile">
                        <img src={tile.img} alt={tile.title} />
                        <GridListTileBar
                            title={tile.title}
                            classes={{
                                root: 'title-gradient-bottom',
                                title: 'text-white',
                            }}
                            actionIcon={
                                <IconButton>
                                    <StarBorderIcon className="text-white" />
                                </IconButton>
                            }
                        />
                    </GridListTile>,
                )}
            </GridList>
        </div>
    );
}

export default SingleLineGridList;