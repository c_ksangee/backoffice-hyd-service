import React from 'react';
import {CartesianGrid, Line, LineChart, ResponsiveContainer, XAxis, YAxis} from 'recharts';
import {chartData, countryList} from "variables/general";
import CountryListItem from "./CountryListItem";
import { ItemGrid } from 'components';
import { Grid } from "@material-ui/core";
const SiteVisitor = () => {
    return (
        <div>
            <h4 className="mb-2 text-muted countriesText">Countries</h4>
            <Grid container>
            <ItemGrid xs={12} sm={6} md={4}>
            {countryList.map((country, index) => <CountryListItem key={index} country={country}/>)}
            </ItemGrid>
            <ItemGrid xs={12} sm={6} md={8}>
            <ResponsiveContainer width="100%" height={150}>
                        <LineChart data={chartData}>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Line type="monotone" dataKey="pv" stroke="#3367d6"
                                  fill="#3367d6"/>
                            <Line type="monotone" dataKey="uv" stroke="#f3b439"
                                  fill="#f3b439"/>
                        </LineChart>
                    </ResponsiveContainer>
            </ItemGrid>
            </Grid>
        </div>
    )
};

export default SiteVisitor;