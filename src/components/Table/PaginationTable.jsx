import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import {Table, TableBody,TableCell,TableFooter,TableHead,TablePagination,TableRow,TableSortLabel,Paper,Tooltip}  from '@material-ui/core';

class EnhancedTableHead extends React.Component {

    createSortHandler = (property, isnumeric) => event => {
        this.props.onRequestSort(event, property, isnumeric);
    };

    render() {
        const { order, orderBy, columnData, classes } = this.props;
        return (
            <TableHead>
                <TableRow className={classes.tableHead}>
                    {columnData.map(column => {
                        return (
                            <TableCell
                                key={column.id}
                                sortDirection={orderBy === column.id ? order : false}
                                style={column.style}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === column.id}
                                        direction={order}
                                        onClick={this.createSortHandler(column.id, column.numeric)}
                                    >
                                        <span className={classes.tableHead}>{column.label}</span>
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string,
    columnData: PropTypes.array.isRequired,
    classes: PropTypes.object
};

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        boxShadow : 'none'
    },
    tableSection: {
        minWidth: 800,
    },
    tableWrapper: {
        overflowX: 'auto',
        border: '1px solid #212529'
    },
    tableHead : {
        fontWeight: 700,
        fontSize:'13px',
        color:'#FFF',
        backgroundColor:'#212529'
    },
    tablefoot : {
        fontWeight: 700,
        fontSize:'13px',
        color:'#000',
    }
});

class PaginationTable extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            selected: -1,
            bodyData: this.props.bodyData,
            orderBy: this.props.orderBy,
            page: 0,
            rowsPerPage: this.props.rowsPerPage || 10,
        };
    }

    handleRequestSort = (event, property, isnumeric) => {
        const orderBy = property;
        const order = (this.state.order === 'desc') ? 'asc' : 'desc';

        if (property === '' || property === undefined)
            property = this.props.orderBy;

        const bodyData =
            (isnumeric) ?
                (order === 'desc'
                    ? this.props.bodyData.sort((a, b) => (parseInt(a[orderBy], 10) > parseInt(b[orderBy], 10) ? -1 : 1))
                    : this.props.bodyData.sort((a, b) => (parseInt(a[orderBy], 10) < parseInt(b[orderBy], 10) ? -1 : 1))
                )
            :
                (order === 'desc'
                    ? this.props.bodyData.sort((a, b) => (a[orderBy] > b[orderBy] ? -1 : 1))
                    : this.props.bodyData.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1))
                )

        this.setState({ bodyData, order, orderBy });
    };

    handleClick = (event, id) => {
        this.setState({ selected: id }, this.props.onhandleClickCallback(id) );
    };

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    isSelected = id => this.state.selected === id;

    render() {
        const { classes, columnData, keyColumn } = this.props;
        const { order, rowsPerPage, page, bodyData, orderBy } = this.state;
        //const emptyRows = rowsPerPage - Math.min(rowsPerPage, bodyData.length - page * rowsPerPage);
        const emptyRows = '';
        return (
            <Paper className={classes.root}>
                <div className={classes.tableWrapper}>
                    <Table className={classes.tableSection}>
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={(a,b,c) => this.handleRequestSort(a,b,c)}
                            columnData={columnData}
                            classes={classes}
                            keyColumn={keyColumn}
                        />
                        <TableBody>
                            {bodyData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                const isSelected = this.isSelected(n[keyColumn]);
                                
                                return (
                                    <TableRow
                                        hover
                                        onClick={event => this.handleClick(event, n[keyColumn])}
                                        role="checkbox"
                                        aria-checked={isSelected}
                                        tabIndex={-1}
                                        key={`keyColumn${n[keyColumn]}`}
                                        selected={isSelected}
                                    >
                                        {
                                            columnData.map(column => {
                                                return <TableCell style={column.style} key={`${column.id}${n[keyColumn]}`}>
                                                    {
                                                        (typeof n[column.id] !== 'string') ? JSON.stringify(n[column.id]) : n[column.id]
                                                    }
                                                </TableCell>
                                            })
                                        }
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                        {
                            (rowsPerPage > 1) &&
                            <TableFooter className={classes.tablefoot}>
                                <TableRow>
                                    <TablePagination
                                        colSpan={6}
                                        count={bodyData.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        backIconButtonProps={{
                                            'aria-label': 'Previous Page',
                                        }}
                                        nextIconButtonProps={{
                                            'aria-label': 'Next Page',
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        }
                    </Table>
                </div>
            </Paper>
        );
    }
}

PaginationTable.propTypes = {
    classes: PropTypes.object.isRequired,
    bodyData: PropTypes.array.isRequired,
    columnData: PropTypes.array.isRequired,
    orderBy: PropTypes.string,
    onhandleClickCallback: PropTypes.func.isRequired
};

export default withStyles(styles)(PaginationTable);