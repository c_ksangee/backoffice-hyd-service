import React from 'react';
import { TagCloud } from "react-tagcloud";

const data = [
  { value: "Index Mang", count: 40 },
  { value: "Content Mang", count: 45 },
  { value: "Image Upload", count: 50 },
  { value: "Product Mang", count: 25 },
  { value: "Approvals", count: 33 },
  { value: "Requests", count: 55 },
  { value: "Users Mang", count: 22 },
  { value: "Partners Mang", count: 20 },
  { value: "Price Range", count: 39 },
  { value: "Website Cache", count: 21 },
  { value: "Sub Affilate", count: 18 },
  { value: "Store front", count: 35 },
  { value: "Pixels", count: 36 },
  { value: "Advanced Search", count: 38 },
  { value: "Configure Content", count: 52 },
  { value: "FTD Suggestions", count: 26 },
  { value: "Ariba", count: 43 },
  { value: "Promotion Programs", count: 23 },
  { value: "Promotion Pulldown", count: 17 },    
  { value: "Site Admin Access", count: 41 },
  { value: "Personal Accounts", count: 24 },
  { value: "Daily Units Sold", count: 33 },
  { value: "Control Page", count: 56 },
  { value: "Shipping Key", count: 28 },
  { value: "Vendor List", count: 29 },
  { value: "Ominature", count: 46 },
  { value: "USAA", count: 44 },
  { value: "Traffic Distribution", count: 16 },
  { value: "Product Lookup", count: 58 },
  { value: "Tabs Mang", count: 47 },
  { value: "Flush Akamai", count: 27 },
];
class SimpleTagCloud extends React.Component{
    render(){
        return(
        <TagCloud minSize={12}
            maxSize={35}
            tags={data}
            onClick={tag => console.log(`'${tag.value}' was selected!`)} />
        )
    }
}

export default SimpleTagCloud;