import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Admin from 'layouts/Admin';
import Login from 'layouts/Login';

class App extends Component {

    isAuthUser = () => {
        if (typeof sessionStorage.getItem('role') !== 'undefined' && sessionStorage.getItem('role') !== '' && sessionStorage.getItem('role') !== null) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <Switch>
                <Route path="/" render={(props) => {
                    return (this.isAuthUser()) ? <Admin {...props} /> : <Login />
                }} />
            </Switch>
        );
    }
}

export default App;