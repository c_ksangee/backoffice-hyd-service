
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Button, IconButton, Card, CardContent } from '@material-ui/core';
import { FormControl, FormHelperText, Input, InputLabel, InputAdornment } from '@material-ui/core';
import { Select, MenuItem } from '@material-ui/core';

import logo from "assets/img/ftd.png";
import { Redirect } from 'react-router-dom'
import { AccountCircle, Visibility, Public } from '@material-ui/icons';
import { getAuthToken } from 'library/actions/authentication.jsx';

import loginStyle from "assets/jss/material-dashboard-react/loginStyle.jsx";

class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      site: '',
      redirect: false,
      formTouched: false,
      errors: {
        username: '',
        password: '',
        site: '',
        auth: ''
      }
    }
  }

  loginSubmit = () => {

    ["username", "password", "site"].map((val) => {
      return this.validateUser(val);
    });

    if (this.state.errors['username'] === '' &&
      this.state.errors['password'] === '' &&
      this.state.errors['site'] === '') {

      getAuthToken(this.state.username, this.state.password, this.state.site)
        .then((response) => {

          if (response.status === 200) {
            response.json().then((res) => {

              if (typeof res.authToken === 'object' && res.authToken !== null) {
                sessionStorage.setItem('role', res.authToken.role);
                sessionStorage.setItem('site', this.state.site);
                sessionStorage.setItem('name', this.state.username);
                this.setState({ 'redirect': true });
              } else {
                this.setState(
                  {
                    'errors': Object.assign(this.state.errors, { auth: 'Authentication failed' })
                  });
              }

            });

          } else {
            this.setState(
              {
                'errors': Object.assign(this.state.errors, { auth: 'Authentication failed' })
              });
          }

        }, (reject) => {
          this.setState(
            {
              'errors': Object.assign(this.state.errors, { auth: 'Authentication failed' })
            });
        });
    }

  }

  handleChange = (event) => {

    const { name, value } = event.target;
    this.setState({
      'formTouched': true,
      'errors': Object.assign(this.state.errors, { [name]: '' }),
      [name]: value
    }, () => { this.validateUser(name) });
  }


  validateUser = (fieldName) => {

    if ((this.state[fieldName] === "")) {
      const errorObj = {
        [fieldName]: "This field is required"
      };
      const newObj = Object.assign(this.state.errors, errorObj);

      this.setState({ 'errors': newObj });
    }

  }

  componentWillMount() {
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('site');
    sessionStorage.removeItem('name');
  }

  render() {

    const { classes } = this.props;
    if (this.state.redirect) {
      return <Redirect to={{
        pathname: '/dashboard',
        state: { username: this.state.username }
      }} />
    }
    return (
      <div className={classes.root}>
        <AppBar position="static" style={{ backgroundColor: "#323232" }}>
          <Toolbar>
            <a href="http://www.ftdcompanies.com" className={classes.logoLink}>
              <div className={classes.logoImage}>
                <img src={logo} alt="logo" className={classes.img} />
              </div>
              FTD Back Office
            </a>
          </Toolbar>
        </AppBar>
        <div className={classes.centeredCardContainer}>
          <div className={classes.centeredCard}>
            <Card className={classes.card} style={{ borderRadius: '7px' }}>
              <CardContent>
                <div className={classes.marginPadding}>
                  <FormControl fullWidth className={classes.margin}>
                    <InputLabel htmlFor="username">Username</InputLabel>
                    <Input
                      id="username"
                      name="username"
                      type="text"
                      error={this.state.errors.username !== ''}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                          >
                            <AccountCircle />
                          </IconButton>
                        </InputAdornment>
                      }
                      onChange={(event) => this.handleChange(event)}
                      onFocus={(event) => this.handleChange(event)}
                    />
                    <FormHelperText error id="username-error-text">{this.state.errors.username}</FormHelperText>
                  </FormControl>

                </div>

                <div className={classes.marginPadding}>
                  <FormControl fullWidth className={classes.margin}>
                    <InputLabel htmlFor="Password">Password</InputLabel>
                    <Input
                      id="Password"
                      name="password"
                      type="password"
                      error={this.state.errors.password !== ''}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                          >
                            <Visibility />
                          </IconButton>
                        </InputAdornment>
                      }
                      onChange={(event) => this.handleChange(event)}
                      onFocus={(event) => this.handleChange(event)}
                    />
                    <FormHelperText error id="password-error-text">{this.state.errors.password}</FormHelperText>
                  </FormControl>
                </div>

                <div className={classes.marginPadding}>
                  <FormControl fullWidth className={classes.margin}>
                    <InputLabel htmlFor="Site">Site</InputLabel>
                    <Select
                      value={this.state.site}
                      onChange={(event) => this.handleChange(event)}
                      onFocus={(event) => this.handleChange(event)}
                      inputProps={{ name: "site", id: "Site" }}
                      error={this.state.errors.site !== ''}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle site visibility"
                          >
                            <Public />
                          </IconButton>
                        </InputAdornment>
                      }
                    >
                      <MenuItem value="FTD">FTD</MenuItem>
                      <MenuItem value="PROFLOWERS">ProFlowers</MenuItem>
                      <MenuItem value="INTERFLORA">Interflora</MenuItem>
                      <MenuItem value="FLORIST">Florist</MenuItem>
                      <MenuItem value="PARTNER">Partner</MenuItem>
                    </Select>
                    <FormHelperText error id="site-error-text">{this.state.errors.site}</FormHelperText>
                  </FormControl>
                </div>
                
                {
                  this.state.errors.auth &&
                  <div className={classes.marginPadding}>
                    <FormControl fullWidth className={classes.margin}>
                      <FormHelperText error id="auth-error-text">{this.state.errors.auth}</FormHelperText>
                    </FormControl>
                  </div>
                }

                <div className={classes.marginPadding}>
                  <Button variant="raised" color="primary"
                    style={{ float: 'right', backgroundColor: "#00acc1" }}
                    onClick={() => this.loginSubmit()}>
                    Login
                   </Button>
                </div>
              </CardContent>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(loginStyle)(Login);
