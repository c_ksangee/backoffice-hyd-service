import fetchWrapper from "library/api/request";

export const getAuthToken = (userName) => {

    return fetchWrapper("authenticate", { "userName" : userName }, {
        method: "GET"
    }    
    , 10000);
}

export const checkAuthStatus = () => {
    
}