import fetchWrapper from "library/api/request";

export const getFeesListBySiteAndChannel = (siteId, channelId, feeCode) => {

    return fetchWrapper("deliveryFees", { "siteId" : siteId, "param" : "search" }, {
        method: "POST",
        agent: false,
        headers: new Headers( {
            "Content-Type": "application/json"            
        } ),
        body: JSON.stringify({ "siteId" : siteId, "channel" : channelId, "feeCode" : feeCode })
    }    
    , 10000);

}

//mock
export const getFeesListBySite = (siteId) => {

    return fetchWrapper("deliveryFees", {}, {
        method: "GET"
    }    
    , 10000, true);

}

export const getFeesByIdMock = (feesList, feeId) => {
    return new Promise((resolve, reject) => {
        var found = feesList.find( 
            ele => ele.feeId === feeId
        )

        if (found)
            resolve(found);
        else 
            reject();
    });    
}


export const createFees = (siteId, data) => {
    
    return fetchWrapper("deliveryFees", { "siteId" : siteId }, {
        agent: false,
        method: "POST",
        headers: new Headers( {
            "Content-Type": "application/json"            
        } ),
        body: JSON.stringify({ "siteId": siteId, "feeEntity": data })
    }, 10000, true)
}

export const updateFees = (siteId, data) => {

    if(1) {
        return new Promise((resolve, reject) => {
            resolve(
                {
                    status: 200
                }
            )
        });
    }
    return fetchWrapper("deliveryFees", { "siteId" : siteId, "param" : data.feeId }, {
        agent: false,
        method: (typeof data.feeId !== 'undefined') ? "PUT" : "POST",
        headers: new Headers( {
            "Content-Type": "application/json"            
        } ),
        body: JSON.stringify({ "siteId": siteId, "feeEntity": data })
    }, 10000)
    
}

export const getFeesById = (siteId, feeId) => {

    return fetchWrapper( "deliveryFees", { "siteId" : siteId, "param" : feeId }, {
        method: "GET",
        agent: false,
    }, 10000);

}

export const deleteFeesById = (siteId, feeId) => {

    return fetchWrapper( "deliveryFees", { "siteId" : siteId, "param" : feeId }, {
        method: "DELETE",
        agent: false,
    }, 10000, true);

}