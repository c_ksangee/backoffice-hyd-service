import fetchWrapper from 'library/api/request';

export const getOrdersByQueueName = (queueName) => {

    return fetchWrapper( 'orderQueue', queueName, {}, 10000);

}

export const getOrderDetailsById = (orderId) => {

    return fetchWrapper('order', '/' + orderId, {}, 10000);

}

export const updateOrder = (data) => {

    data.deliveryDetails.productPrice = parseFloat(data.orderAmount.retailProductAmount).toFixed(2);
    data.orderAmount.saleProductAmount = (parseFloat(data.orderAmount.retailProductAmount) - parseFloat(data.orderAmount.discountAmount)).toFixed(2);

    return fetchWrapper('order', '/', {
        agent: false,
        method: 'PUT',
        headers: new Headers( {
            'Content-Type': 'application/json'            
        } ),
        body: JSON.stringify(data)
    }, 10000)
    
}
