import fetchWrapper from "library/api/request";

export const getPolicySet = (siteId, entityName, role) => {

    return fetchWrapper("policySet", { "siteId" : siteId, "entityName" : entityName, "role" : role }, {
        method: "GET",
        agent: false
    }
    , 10000);
}

export const getActionSet = ( entityName, actionSetId) => {
    
    return fetchWrapper("policyActionSet", { "entityName" : entityName, "actionSetId" : actionSetId }, {
        agent: false,
        method: "GET"
    }, 
    10000);
}


export const getPolicySetByEntity = (entityName) => {

    return fetchWrapper("policySetByEntity", { "entityName" : entityName }, {
        method: "GET",
        agent: false
    }
    , 10000);

}

export const createPolicySet = (policyData) => {
    return fetchWrapper("createPolicy", {}, {
        agent: false,
        method: "POST",
        headers: new Headers( {
            "Content-Type": "application/json"            
        } ),
        body: JSON.stringify({ ...policyData })
    }, 10000, true)

}