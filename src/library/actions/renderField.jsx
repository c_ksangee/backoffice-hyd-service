import React from "react";
import { TextField, Radio, RadioGroup, Input, Checkbox, Select, MenuItem, FormControl, InputLabel, FormControlLabel, InputAdornment } from "@material-ui/core";

const styles = {
    marginTop: {
        margin: '20px 0'
    }
}
const renderRestrictedField = (
    { label, classes },
) => (
        <div className={classes.table}>
            <div className={classes.tableRow}>
                <div className={classes.tableCell}>
                    <label style={{ color: "#000", lineHeight: 2 }}>{label}</label>
                </div>
                <div className={classes.tableCell}>
                    <label>N/A</label>
                </div>
            </div>
        </div>
    );

const renderStringField = (
    { input, ...custom, classes },
) => (
        <label>{input.value || custom.default}</label>
    );

const renderLabelField = (
    { input, label, ...custom, classes },
) => (
        (custom.displayType === 'material-ui') ?
            <FormControl fullWidth style={styles.marginTop}>
                <InputLabel htmlFor={label}>{label}</InputLabel>
                <Input
                    {...input}
                    disabled
                />
            </FormControl>
            :
            <div className={classes.table}>
                <div className={classes.tableRow}>
                    <div className={classes.tableCell}>
                        <label style={{ color: "#000", lineHeight: 2 }}>{label}</label>
                    </div>
                    <div className={classes.tableCell}>
                        <label>
                            {
                                (custom.mask ?
                                    `***********${String.prototype.slice.call(input.value, 12)}`
                                    :
                                    (custom.valueType === 'CUR') ?
                                        "$" + parseFloat(input.value).toFixed(2)
                                        :
                                        input.value || custom.default
                                )
                            }
                        </label>
                    </div>
                </div>
            </div>
    );

const renderTextField = (
    { input, label, meta: { touched, error }, ...custom, classes },
) => (
        (custom.displayType === 'material-ui') ?
            <FormControl fullWidth style={styles.marginTop}>
                <InputLabel htmlFor={label}>{label}</InputLabel>
                <Input
                    {...input}
                    error={touched && error}
                />
            </FormControl>
            :

            <div className={classes.table}>
                <div className={classes.tableRow}>
                    <div className={classes.tableCell}>
                        <label style={{ color: "#000" }}>{label}</label>
                    </div>
                    <div className={classes.tableCell}>
                        <TextField fullWidth {...input}
                            error={(touched && error) ? true : false}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">{(custom.valueType === 'CUR') ? "$" : ""}</InputAdornment>,
                                maxLength: custom.maxlength
                            }}
                        />
                    </div>
                </div>
            </div>
    );

const renderTextAreaField = (
    { input, label, ...custom, classes },
) => (
        <div className={classes.table}>
            <div className={classes.tableRow}>
                <div className={classes.tableCell}>
                    <label style={{ color: "#000" }}>{label}</label>
                </div>
                <div className={classes.tableCell}>
                    <TextField fullWidth {...input} rows={2} rowsMax={4} />
                </div>
            </div>
        </div>
    );

const renderCheckbox = ({ input, label, classes }) => (
    <div className={classes.table}>
        <div className={classes.tableRow}>
            <div className={classes.tableCell}>
                <label style={{ color: "#000" }}>{label}</label>
            </div>
            <div className={classes.tableCell}><Checkbox
                label={label}
                checked={input.value ? true : false}
                onCheck={input.onChange}
            />
            </div>
        </div>
    </div>
);

const renderRadioGroup = ({ input, label, options, classes, ...rest }) => (
    <div className={classes.table}>
        <div className={classes.tableRow}>
            <div className={classes.tableCell}>
                <label style={{ color: "#000" }}>{label}</label>
            </div>
            <div className={classes.tableCell}>
                <RadioGroup
                    {...input}
                    valueselected={input.value}
                    onChange={(event, value) => input.onChange(event, value)}
                    style={{ display: (rest.mode === 'horizontal') ? "inline" : "flex" }}
                >
                    {options.map(val => <FormControlLabel key={val} control={<Radio color="primary" />} value={val} label={val} />)}
                </RadioGroup>
            </div>
        </div>
    </div>
);

const renderImageField = (
    { input, label, ...custom },
) => (
        <img src={input.value} alt={label} {...custom} />
    );

const renderSelectField = (
    { input, label, meta: { touched, error }, children, classes, options, ...custom },
) => (
        (custom.displayType === 'material-ui') ?
            <FormControl fullWidth style={styles.marginTop}>
                <InputLabel htmlFor={label}>{label}</InputLabel>
                <Select
                    {...input}
                    onChange={(event) => input.onChange(event.target.value)}
                    error={touched && error}
                >
                    {options.map(val => <MenuItem value={val} primaryText={val}>{val}</MenuItem>)}
                </Select>
            </FormControl>
            :
            <div className={classes.table}>
                <div className={classes.tableRow}>
                    <div className={classes.tableCell}>
                        <label style={{ color: "#000" }}>{label}</label>
                    </div>
                    <div className={classes.tableCell}>
                        <Select
                            floatingLabelText={label}
                            error={touched && error}
                            {...input}
                            onChange={(event) => input.onChange(event.target.value)}
                            onBlur={undefined}
                            {...custom}>
                            {options.map(val => <MenuItem value={val} primaryText={val}>{val}</MenuItem>)}
                        </Select>
                    </div>
                </div>
            </div>
    );


const renderField = (props) => {

    const action = (typeof props.acl === 'object') ? props.acl[props.input.name] || props.acl.default : 'NO_ACCESS';

    if (action === 'NO_ACCESS') {
        return renderRestrictedField(props);
    }

    const renderType = (action === 'READ_ONLY' && props.render_type !== 'string' && props.render_type !== 'image') ? 'label' : props.render_type;

    switch (renderType) {
        case 'string':
            return renderStringField(props);
        case 'label':
            return renderLabelField(props);
        case 'image':
            return renderImageField(props);
        case 'checkbox':
            return renderCheckbox(props);
        case 'radio':
            return renderRadioGroup(props);
        case 'textarea':
            return renderTextAreaField(props);
        case 'selectfield':
            return renderSelectField(props);
        default:
            return renderTextField(props);
    }
}

export default renderField;