export const setCookie = (c_name, c_value, exdays) => {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    return encodeURIComponent(c_name)
        + "=" + encodeURIComponent(c_value)
        + (!exdays ? "" : "; expires=" + exdate.toUTCString());
    ;
}