import services from 'library/api/services';

const fetchWrapper = (name, params, options, timeout, mock) => {

    const url = services(name, params, mock);
    return new Promise((resolve, reject) => {
        fetch(url, options).then(resolve).catch(reject);

        if (timeout) {
            const e = new Error("Connection timed out");
            setTimeout(reject, timeout, e);
        }        
    });
}

export default fetchWrapper;