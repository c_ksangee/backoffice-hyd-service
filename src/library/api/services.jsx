/**
 * Handles configuring of API URLs.
 *
 * @param {String} name The keyname of the service.
 * @param {String} params The params required for the service.
 *
 * @example
 *  import service from './services
 *  const serviceOne = service( 'product', `${id}/${something}` );
 *  const serviceTwo = service( 'search', `?query=${term}&num=${count}` );
 *
 * @returns {String}
 */

const NODE_PROTOCOL="http";
const NODE_MICRO_SERVICE_DOMAIN="nonprod-gke-primary-1-proxy-qa1.gcp.ftdi.com";
const NODE_ORDER_SERVICE_DOMAIN = "10.82.216.90";
const NODE_AUTHZ_DOMAIN = "10.82.216.85";
const NODE_NETFLIXUI_DOMAIN="10.82.216.87";
const NODE_AUTHN_DOMAIN="10.82.216.85";

const protocol = `${ NODE_PROTOCOL }://`;
const baseDomain = `${ protocol }${ NODE_MICRO_SERVICE_DOMAIN }`;

export default ( name, params = '', mock ) => {
    
    if (mock) {

        const mock_services = {
            'deliveryFees' : `/mock/delivery-fees.json`,
            'policySet' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/getpolicyset/${params.siteId}/${params.entityName}/${params.role}`,
            'policyActionSet' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/getactionset/${params.entityName}/${params.actionSetId}`,
        };

        return mock_services[ name ];
    }

    const services = {
        'orderQueue': `${ protocol }${ NODE_ORDER_SERVICE_DOMAIN }/order-status/${params}`,
        'order': `${ protocol }${ NODE_ORDER_SERVICE_DOMAIN }/order${params}`,
        'workflow': `${ protocol }${ NODE_NETFLIXUI_DOMAIN }/#/workflow/id/${params}?VWF=1`    ,
        'deliveryFees' : `${ baseDomain }/fees-admin-service/${params.siteId}/api/fees-admin/fee/${params.param}`,
        'policySet' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/getpolicyset/${params.siteId}/${params.entityName}/${params.role}`,
        'policySetByEntity' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/getpolicyset/${params.entityName}`,
        'policyActionSet' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/getactionset/${params.entityName}/${params.actionSetId}`,
        'createPolicy' : `${ protocol }${ NODE_AUTHZ_DOMAIN }/apis/addpolicyset/`,
        'authenticate' : `${ protocol }${ NODE_AUTHN_DOMAIN }/apis/getauthtokenbyusername/${params.userName}`
    };
    
    return services[ name ];
};
