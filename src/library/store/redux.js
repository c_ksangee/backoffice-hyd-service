import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { reducer as reduxFormReducer } from 'redux-form';

const actionTypes = {
    ADD_TO_STORE: 'ADD_TO_STORE'
};

// REDUCERS
const pageReducer = ( state = {}, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_TO_STORE:
            return {
                ...state,
                ...action.data
            };
        default: return state;
    }
};

const reducer = combineReducers( {
    page: pageReducer,
    form: reduxFormReducer
} );


// ACTION CREATORS
export const addToStore = ( data ) => ( dispatch ) => {
    return dispatch( {
        type: actionTypes.ADD_TO_STORE,
        data
    } );
};

const store = createStore(
    reducer,
    {},
    applyMiddleware( thunkMiddleware )
);

export default store;