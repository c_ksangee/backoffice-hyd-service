import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import TableList from "views/TableList/TableList.jsx";
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import Maps from "views/Maps/Maps.jsx";
import NotificationsPage from "views/Notifications/Notifications.jsx";

import AccessControl from "views/AccessControl";
import Support from "views/Support";
import Content from "views/Content";
import Catalog from "views/Catalog";
import DeliveryFee from "views/DeliveryFee";
import SiteControls from "views/SiteControls";
import Reports from "views/Reports";

import {
  Dashboard,
  Person,
  ContentPaste,
  LibraryBooks,
  BubbleChart,
  LocationOn,
  Notifications,
  Search,
  Widgets,
  ContactPhone,
  AttachMoney
} from "@material-ui/icons";

const dashboardRoutes = [
  {
    path: "/dashboard",
    sidebarName: "Dashboard",
    navbarName: "Dashboard",
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: "/admincontrol",
    sidebarName: "Access Control",
    navbarName: "Access Control",
    icon: Person,
    component: AccessControl
  },
  {
    path: "/deliveryfee",
    sidebarName: "Delivery Fee",
    navbarName: "Delivery Fee",
    icon: AttachMoney,
    component: DeliveryFee
  },
  {
    path: "/catalog",
    sidebarName: "Catalog",
    navbarName: "Catalog",
    icon: LibraryBooks,
    component: Catalog
  },
  {
    path: "/support",
    sidebarName: "Customer Care",
    navbarName: "Customer Care",
    icon: ContactPhone,
    component: Support
  },
  {
    path: "/sitecontrols",
    sidebarName: "Site Controls",
    navbarName: "Site Controls",
    icon: Search,
    component: SiteControls
  },
  {
    path: "/content",
    sidebarName: "Site Content",
    navbarName: "Site Content",
    icon: Widgets,
    component: Content
  },
  {
    path: "/reports",
    sidebarName: "Reports",
    navbarName: "Reports",
    icon: ContentPaste,
    component: Reports
  },
  {
    path: "/user",
    sidebarName: "User Profile",
    navbarName: "Profile",
    icon: Person,
    component: UserProfile
  },
  {
    path: "/table",
    sidebarName: "Table List",
    navbarName: "Table List",
    icon: ContentPaste,
    component: TableList
  },
  {
    path: "/typography",
    sidebarName: "Typography",
    navbarName: "Typography",
    icon: LibraryBooks,
    component: Typography
  },
  {
    path: "/icons",
    sidebarName: "Icons",
    navbarName: "Icons",
    icon: BubbleChart,
    component: Icons
  },
  {
    path: "/maps",
    sidebarName: "Maps",
    navbarName: "Map",
    icon: LocationOn,
    component: Maps
  },
  {
    path: "/notifications",
    sidebarName: "Notifications",
    navbarName: "Notifications",
    icon: Notifications,
    component: NotificationsPage
  },
  { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default dashboardRoutes;
