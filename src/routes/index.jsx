import App from "layouts/App";
import Login from "layouts/Login";

const indexRoutes = [
    { path: "/login", component: Login },
    { path: "/", component: App }    
];

export default indexRoutes;
