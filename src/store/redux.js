import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunkMiddleware from 'redux-thunk';

export const actionTypes = {
    ADD_TO_STORE: 'ADD_TO_STORE'
};

// REDUCERS
export const pageReducer = ( state = {}, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_TO_STORE:
            return {
                ...state,
                ...action.data
            };
        default: return state;
    }
};

export const reducer = combineReducers( {
    page: pageReducer,
    form: formReducer
} );

// ACTIONS
export const addToStore = ( data, component ) => ( dispatch ) => {
    return dispatch( {
        type: actionTypes.ADD_TO_STORE,
        data: { component: data }
    } );
};

export const initStore = createStore( reducer, {}, applyMiddleware( thunkMiddleware ) );