export default [
    {
      id: 0,
      title: '10 Orders',
      allDay: true,
      start: new Date(2018, 5, 1),
      end: new Date(2018, 5, 2),
    },
    {
      id: 1,
      title: '3 Orders',
      start: new Date(2018, 5, 7),
      end: new Date(2018, 5, 7),
    },
  
    {
      id: 2,
      title: '1 Order',
      start: new Date(2018, 6, 13, 0, 0, 0),
      end: new Date(2018, 6, 13, 0, 0, 0),
    },
  
    {
      id: 3,
      title: '6 Orders',
      start: new Date(2018, 6, 6, 0, 0, 0),
      end: new Date(2018, 6, 6, 0, 0, 0),
    },
  
    {
      id: 4,
      title: '15 Orders',
      start: new Date(2018, 7, 9, 0, 0, 0),
      end: new Date(2018, 7, 9, 0, 0, 0),
    },
    {
      id: 5,
      title: '14 Orders',
      start: new Date(2018, 8, 11),
      end: new Date(2018, 8, 11),
      desc: 'Big conference for important people',
    },
    {
      id: 6,
      title: '21 Orders',
      start: new Date(2018, 9, 12, 10, 30, 0, 0),
      end: new Date(2018, 9, 12, 12, 30, 0, 0),
      desc: 'Pre-meeting meeting, to prepare for the meeting',
    },
    {
      id: 7,
      title: '12 Orders',
      start: new Date(2018, 5, 16),
      end: new Date(2018, 5, 16 ),
      desc: 'Power lunch',
    },
    {
      id: 8,
      title: '14 Orders',
      start: new Date(2018, 5, 21),
      end: new Date(2018, 5, 21),
    },
    {
      id: 9,
      title: '2 Orders',
      start: new Date(2018, 5, 12, 17, 0, 0, 0),
      end: new Date(2018, 5, 12, 17, 30, 0, 0),
      desc: 'Most important meal of the day',
    },
    {
      id: 10,
      title: '8 Orders',
      start: new Date(2018, 6, 12, 20, 0, 0, 0),
      end: new Date(2018, 6, 12, 21, 0, 0, 0),
    },
    {
      id: 11,
      title: '10 Orders',
      start: new Date(2018, 5, 13, 7, 0, 0),
      end: new Date(2018, 5, 13, 10, 30, 0),
    },
    {
      id: 12,
      title: 'Late Night Event - 5 Orders',
      start: new Date(2018, 10, 17, 19, 30, 0),
      end: new Date(2018, 10, 17, 2, 0, 0),
    },
    {
      id: 13,
      title: 'Multi-day Event - 25 Orders',
      start: new Date(2018, 11, 20, 19, 30, 0),
      end: new Date(2018, 11, 22, 2, 0, 0),
    },
    {
      id: 14,
      title: 'Today',
      start: new Date(new Date().setHours(new Date().getHours() - 3)),
      end: new Date(new Date().setHours(new Date().getHours() + 3)),
    },
    {
      id: 15,
      title: '25 Orders',
      start: new Date(2018, 5, 24, 19, 30, 0),
      end: new Date(2018, 5, 24, 2, 0, 0),
    }
  ]