// ##############################
// // // Tasks for TasksCard - see Dashboard view
// #############################

var bugs = [
  'Sign contract for "What are conference organizers afraid of?"',
  "Lines From Great Russian Literature? Or E-mails From My Boss?",
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  "Create 4 Invisible User Experiences you Never Knew About",
  "Order : ORD3459234 ITEM #B07 - The FTD® Best Day™ Bouquet- VASE INCLUDED"
];
var website = [
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];
var server = [
  "Lines From Great Russian Literature? Or E-mails From My Boss?",
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];
var reqtasks = [
  'Add Product Request - #SMRB78',
  "Content Change for the control - header_responsive",
  "Add Index Request - Best_Sellers_Mothers_Day",
  "User Management - New User request for CMS - sbravo",
  "Image Upload Management - New Banner Image uploaded",
  "Image Upload Management - New Header Image uploaded"
];
var approvedreq = [
  "Content Change for the control - footer_responsive",
  '"Content Change for the control - product_cont"',
  "Add Product Request - #SMRB75",
  "Image Upload Management - New Header Image uploaded"
];
var rejectedreq = [
  "Add Product Request - #SMRB90",
  "User Management : New User request for CMS - ssssuing",
  'User Management : New User request for CMS - biniths',
  "Image Upload Management - New Header Image uploaded"
];
var process = [
 "Order : ORD3459234 ITEM #B07 - The FTD® Best Day™ Bouquet- VASE INCLUDED",
 "Order : ORD3423445 ITEM #FMTP - Rush of Color Assorted Tulip Bouquet",
 "Order : ORD3452346 ITEM #B05 - The Sunny Sentiments™ Bouquet by FTD® - VASE INCLUDED",
 "Order : ORD3434534 ITEM #FMMS - Wistful Wishes Gilliflower Bouquet",
 "Order : ORD3454566 ITEM #P1378 - Living Succulent Wreath",
 "Order : ORD3453453 ITEM #FAC1 - Wild & Free"
];
var accepted = [
  "Order : ORD3459234 ITEM #B07 - The FTD® Best Day™ Bouquet- VASE INCLUDED",
  "Order : ORD3423445 ITEM #FMTP - Rush of Color Assorted Tulip Bouquet",
  "Order : ORD3452346 ITEM #B05 - The Sunny Sentiments™ Bouquet by FTD® - VASE INCLUDED",
  "Order : ORD3434534 ITEM #FMMS - Wistful Wishes Gilliflower Bouquet",
  "Order : ORD3454566 ITEM #P1378 - Living Succulent Wreath",
  "Order : ORD3453453 ITEM #FAC1 - Wild & Free"
];
var rejected = [
  "Order : ORD3459234 ITEM #B07 - The FTD® Best Day™ Bouquet- VASE INCLUDED",
  "Order : ORD3423445 ITEM #FMTP - Rush of Color Assorted Tulip Bouquet",
  "Order : ORD3452346 ITEM #B05 - The Sunny Sentiments™ Bouquet by FTD® - VASE INCLUDED",
  "Order : ORD3434534 ITEM #FMMS - Wistful Wishes Gilliflower Bouquet",
  "Order : ORD3454566 ITEM #P1378 - Living Succulent Wreath",
  "Order : ORD3453453 ITEM #FAC1 - Wild & Free"
];

var appNotification = [
  {
      id: 1,
      title: "New Content Request",
     desc:  "Jeff has added new content for the product #SMRB",
      image: 'http://via.placeholder.com/256x256'
  },
  {
      id: 2,
      title: "Support ticket",
       desc:  "Mario raised a support ticket",
      image: 'http://via.placeholder.com/256x256'
  },
  {
      id: 3,
      title: "New Product Request",
     desc: "Web Support Team added new product #TTI05",
      image: 'http://via.placeholder.com/256x256'
  },
  {
      id: 4,
      title: "New MarkCode",
      desc: "Jaanu raised a support ticket for new Markcode request for partner",
      image: 'http://via.placeholder.com/256x256'
  },
];

var announcementsNotification = [
  {
    id: 1,
    title: "New Banner Request",
   desc:  "John has added new content for the product #SMRB",
    image: 'http://via.placeholder.com/256x256'
},
{
    id: 2,
    title: "New Index Management ticket",
     desc:  "Mano raised a support ticket",
    image: 'http://via.placeholder.com/256x256'
},
{
    id: 3,
    title: "New Product Sku Request",
   desc: "Web Support Team added new product #UUI05",
    image: 'http://via.placeholder.com/256x256'
},
{
    id: 4,
    title: "New User Request",
    desc: "Ramky raised a support ticket for new user creation request for partner",
    image: 'http://via.placeholder.com/256x256'
},
];

var expanseData = [
  {name: 'Jan', Points: 10, Expanse: 22},
  {name: 'Feb', Points: 5, Expanse: 20},
  {name: 'March', Points: 15, Expanse: 26},
  {name: 'April', Points: 13, Expanse: 22},
  {name: 'May', Points: 35, Expanse: 26},
  {name: 'June', Points: 25, Expanse: 20},
  {name: 'July', Points: 20, Expanse: 32},
  {name: 'Aug', Points: 25, Expanse: 35}
];

var chartData = [
  {name: '6.00', uv: 500, pv: 1100, amt: 20},
  {name: '7.00', uv: 600, pv: 1600, amt: 21},
  {name: '8.00', uv: 500, pv: 1900, amt: 29},
  {name: '9.00', uv: 600, pv: 2100, amt: 20},
  {name: '10.00', uv: 700, pv: 2500, amt: 28},
  {name: '11.00', uv: 800, pv: 2200, amt: 20},
  {name: '12.00', uv: 700, pv: 2000, amt: 20},
  {name: '13.00', uv: 800, pv: 1900, amt: 20},
  {name: '14.00', uv: 850, pv: 1700, amt: 20},
  {name: '15.00', uv: 1000, pv: 2100, amt: 20},
];

var countryList = [
  {name: 'United States', flagCode: 'us', badge: '27000', badgeColor: 'primary'},
  {name: 'France', flagCode: 'fr', badge: '3200', badgeColor: 'teal'},
  {name: 'Germany', flagCode: 'gm', badge: '1800', badgeColor: 'purple'},
  {name: 'Spain', flagCode: 'es', badge: '2800', badgeColor: 'cyan'},
];

module.exports = {
  // these 3 are used to create the tasks lists in TasksCard - Dashboard view
  bugs,
  website,
  server,
  process,
  accepted,
  rejected,
  appNotification,
  announcementsNotification,
  chartData,
  countryList,
  reqtasks,
  approvedreq,
  rejectedreq,
  expanseData
};
