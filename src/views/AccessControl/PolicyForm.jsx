import React from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import renderField from "library/actions/renderField.jsx";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  CircularProgress,
  Grid
} from "@material-ui/core";
import { PaginationTable, ItemGrid } from "components";

const style = {
  error: {
    textAlign: "center",
    color: "red"
  }
};

const columnList = [
  {
    id: "ruleId",
    numeric: true,
    label: "Rule ID"
  },
  {
    id: "priority",
    numeric: true,
    label: "Priority"
  },
  {
    id: "conditionJson",
    numeric: false,
    label: "Conditions"
  },
  {
    id: "actionJson",
    numeric: false,
    label: "Action"
  }
];

class PolicyForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      policySubmitInProgress: false,
      policyActionType: this.props.initialValues.policy.action,
    };
  }

  doSubmit() {
    this.setState({ policySubmitInProgress: true });

    this.props.handleSubmit().then(resolve => {
      this.setState({ policySubmitInProgress: false });
    });
  }

  handleChange(event, value) {
    this.setState({ policyActionType: event.target.value });
  }

  render() {
    const { classes, initialValues, acl, showError } = this.props;

    return (
      <div className={`${classes.leftAlign}`}>
        {typeof initialValues === "object" ? (
          <div>
            {
              <div>
              {showError !== "" && <div style={style.error}><h4>{showError}</h4></div>}

              {this.state.policySubmitInProgress === true &&
                <div>
                  <CircularProgress
                    size={40}
                    style={{ color: "rgb(202, 173, 89)" }}
                  />
                </div>
              }
            </div>
            }
            <form>
              <div className={`${classes.custTable}`}>
                <div className={`${classes.custTableBody}`}>
                  <div className={`${classes.custTableRow}`}>
                    <div>
                      <Grid container>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="label"
                            classes={classes}
                            label="ID:"
                            name="id"
                            default="<Auto Generated>"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Name:"
                            name="name"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Description:"
                            name="description"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Entity:"
                            name="entityName"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="label"
                            classes={classes}
                            label="Site:"
                            name="policy.resource.siteIds"
                            value={({ data }) =>
                              data.reduce((prev, curr) => {
                                console.log("prev:" + prev + " curr:" + curr);
                                return [prev, ", ", curr];
                              })
                            }
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            component={renderField}
                            render_type="label"
                            classes={classes}
                            label="Role:"
                            name="policy.subject.role"
                            value={({ data }) =>
                              data.reduce((prev, curr) => [prev, ", ", curr])
                            }
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={12} md={4}>
                          <Field
                            displayType="material-ui"
                            acl={acl}
                            onChange={(e, v) => this.handleChange(e, v)}
                            component={renderField}
                            render_type="radio"
                            classes={classes}
                            label="Action:"
                            name="policy.action"
                            options={[
                              "READ_ONLY",
                              "FULL_ACCESS",
                              "NO_ACCESS",
                              "CUSTOM"
                            ]}
                          />
                        </ItemGrid>
                      </Grid>
                    </div>
                  </div>
                </div>
              </div>
              {
                this.state.policyActionType === "CUSTOM" &&
                typeof initialValues.policy.customRules === "object" &&
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan="2">
                        <label>Rules</label>
                        <PaginationTable
                          columnData={columnList}
                          bodyData={initialValues.policy.customRules.rulesList}
                          onhandleClickCallback={this.props.onClick}
                          keyColumn="ruleId"
                          orderBy="ruleId"
                          order="asc"
                        />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              }

            </form>
          </div>
        ) : (
            <p>Unable to fetch policy details. Please try after some time</p>
          )}
      </div>
    );
  }
}

PolicyForm = reduxForm({
  form: "PolicyForm"
})(PolicyForm);

PolicyForm.propTypes = {
  classes: PropTypes.object.isRequired
};

export default PolicyForm;
