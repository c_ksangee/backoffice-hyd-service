import React from "react";
import PropTypes from "prop-types";

import {
  Chip,
  Avatar,
  withStyles,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  CircularProgress,
  Grid
} from "@material-ui/core";

import {
  Button,
  PaginationTable,
  RegularCard,
  ItemGrid
} from "components";

import PolicyForm from "./PolicyForm.jsx";
import PolicyRule from "./PolicyRule.jsx";

import {
  successColor,
  warningColor,
  infoColor
} from "assets/jss/material-dashboard-react.jsx";
import policyStyle from "assets/jss/material-dashboard-react/policyStyle.jsx";

import {
  getPolicySetByEntity,
  createPolicySet
} from "library/actions/policy.jsx";
import { connect } from 'react-redux';
import { submit } from 'redux-form'

const entityNames = [
  {
    name: "product",
    color: successColor,
    code: "product"
  },
  {
    name: "delivery fees",
    color: warningColor,
    code: "fee"
  },
  {
    name: "order",
    color: infoColor,
    code: "order"
  }
];

const columnList = [
  {
    id: "id",
    numeric: false,
    label: "Policy ID"
  },
  {
    id: "name",
    numeric: false,
    label: "Name"
  },
  {
    id: "description",
    numeric: false,
    label: "Description"
  },
  {
    id: "entityName",
    numeric: false,
    label: "Entity"
  }
];

class PolicyList extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.initialState();
  }

  initialState(override) {

    return {
      entityStep: 1,
      title: "List of Access Controls",
      showPolicyList: false,
      showPolicyDetails: false,
      selectedEntity: "",
      selectedRole: sessionStorage.getItem("role"),
      policyList: [],
      selectedPolicy: "",
      showMessage: "",
      showError: "",
      ...override
    };
  }

  entityClickHandler = event => {
    if (typeof event.target.value !== "undefined") {
      this.setState(
        this.initialState({
          selectedEntity: event.target.value,
          selectedRole: this.state.selectedRole
        }),
        this.fetchPolicyList
      );
    }
  };

  fetchPolicyList() {
    if (this.state.selectedEntity === "" || this.state.selectedRole === "")
      return;

    if (this.state.selectedRole !== "SUPER_ADMIN") {
      this.setState({
        showPolicyList: true,
        showMessage:
          "You are not authorized to see this page. Please check with Administrator."
      });
      return;
    }

    getPolicySetByEntity(this.state.selectedEntity).then(
      response => {
        if (response.status === 200) {
          response.json().then(list => {
            this.setState({
              showPolicyList: true,
              policyList: list.policySetList,
              showMessage: ""
            });
          });
        } else {
          this.setState({
            showPolicyList: true,
            showMessage: "Unable to fetch policies. Please try after sometime."
          });
        }
      },
      reject => {
        this.setState({
          showPolicyList: true,
          showMessage:
            "Unable to fetch policies due to an unexpected error. Please try after sometime."
        });
      }
    );
  }

  selectPolicyHandler = policyId => {
    let policyIndex;
    const policy = this.state.policyList.find((v, i) => {
      if (v.id === policyId) {
        policyIndex = i;
        return v;
      }
    });

    this.setState({
      entityStep: 2,
      title:
        typeof policyId !== "undefined" ? "Update Policy" : "Create Policy",
      showPolicyDetails: true,
      selectedPolicy: policy,
      selectedPolicyIndex: policyIndex
    });
  };

  savePolicy(values) {
    return createPolicySet(values).then(
      response => {
        if (response.status === 200) {
          const container = document.querySelector("#mainPanel");
          container.scrollTop = 0;

          this.setState({
            showMessage:
              "Updated policy " +
              this.state.selectedPolicy.name +
              " details successfully."
          });
        } else {
          this.setState({
            showError:
              "Unable to update policy " +
              this.state.selectedPolicy.name +
              " details."
          });
        }
      },
      reject => {
        this.setState({
          showError:
            "Unable to update policy " +
            this.state.selectedPolicy.name +
            " details."
        });
      }
    );
  }

  createPolicyHandler = () => {
    this.setState({
      entityStep: 2,
      showPolicyDetails: true,
      selectedPolicy: {}
    });
  };

  goBack = event => {
    this.setState({
      entityStep: this.state.entityStep - 1,
      title: this.titleSetter(this.state.entityStep - 1)
    });
  };

  titleSetter = entityStep => {
    switch (entityStep) {
      case 1:
        return "List of ACL Policies";
      case 2:
        return "Create or Update Policy";
      case 3:
        return "Edit Policy Rule";
      default:
        return "List of ACL Policies";
    }
  };

  selectRuleHandler = ruleId => {
    let ruleIndex;
    const rule = this.state.selectedPolicy.policy.customRules.rulesList.find(
      (v, i) => {
        if (v.ruleId === ruleId) {
          ruleIndex = i;
          return v;
        }
      }
    );

    this.setState({
      entityStep: 3,
      title: this.titleSetter(3),
      selectedRule: rule,
      selectedRuleIndex: ruleIndex
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <RegularCard
        headerColor="green"
        cardTitle={this.state.title}
        style={{minHeight: '300px'}}
        content={
          <React.Fragment>
            {this.state.entityStep === 1 ? (
              <React.Fragment>
                <div className={classes.table}>
                  <div className={classes.tableRow}>
                    <div className={classes.tableCell}>
                      <FormControl component="fieldset" required>
                        <FormLabel component="legend">Choose Entity:</FormLabel>
                        <RadioGroup
                          name="entityName"
                          value={this.state.selectedEntity}
                          onChange={this.entityClickHandler}
                          style={{
                            display: "inline",
                            textTransform: "uppercase"
                          }}
                        >
                          {entityNames.map(object => {
                            return (
                              <FormControlLabel
                                key={object.name}
                                value={object.code}
                                control={
                                  <Radio style={{ color: object.color }} />
                                }
                                label={object.name}
                              />
                            );
                          })}
                        </RadioGroup>
                      </FormControl>
                    </div>
                  </div>
                </div>

                <div className={classes.centerAlign}>
                  {
                    this.state.selectedEntity !== "" &&
                    this.state.selectedRole !== "" &&
                    (
                      this.state.showPolicyList === false ? (
                        <div className={classes.marginTop}>
                          <CircularProgress size={60} />
                        </div>
                      )
                        :
                        this.state.policyList.length === 0 ? (
                          <Chip
                            avatar={<Avatar />}
                            label={`There are no policies configured for '${
                              this.state.selectedEntity
                              }' entity.`}
                            className={classes.chip}
                          />
                        )
                          :
                          <div>
                            {
                              this.state.showMessage !== "" &&
                              <Chip
                                avatar={<Avatar />}
                                label={this.state.showMessage}
                                className={classes.chip}
                              />
                            }

                            <PaginationTable
                              columnData={columnList}
                              bodyData={this.state.policyList}
                              onhandleClickCallback={id =>
                                this.selectPolicyHandler(id)
                              }
                              keyColumn="id"
                            />
                          </div>
                    )
                  }
                </div>
              </React.Fragment>
            ) : this.state.entityStep === 2 ? (
              <React.Fragment>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={12}>
                    <div className={classes.headSection}>
                      <div className={classes.leftNav}>
                        <Button color="warning" onClick={this.goBack}>
                          Back to List
                        </Button>
                      </div>
                      <div className={classes.rightNav}>
                        {this.state.policySubmitInProgress === true ? (
                          <div>
                            <CircularProgress
                              size={40}
                              style={{ color: "rgb(202, 173, 89)" }}
                            />
                          </div>
                        ) : (
                            <div className={classes.rightAlign}>
                              <Button
                                type="submit"
                                color="info"
                                onClick={this.props.dispatch(submit('PolicyForm'))}
                              >
                                Save Policy
                            </Button>
                            </div>
                          )}
                      </div>
                    </div>
                    <hr className={classes.hrClass} />
                  </ItemGrid>
                </Grid>

                {this.state.showPolicyDetails === false ? (
                  <div
                    className={`${classes.marginTop} ${classes.centerAlign}`}
                  >
                    <CircularProgress
                      className={classes.refreshLoading}
                      size={60}
                    />
                  </div>
                ) : this.state.showMessage !== "" ? (
                  <div
                    className={`${classes.marginTop} ${classes.centerAlign}`}
                  >
                    <Chip
                      avatar={<Avatar />}
                      label={this.state.showMessage}
                      className={classes.chip}
                    />
                  </div>
                ) : (
                      <div>
                        {this.state.showError !== "" ? (
                          <PolicyForm
                            initialValues={this.state.selectedPolicy}
                            classes={classes}
                            onClick={() => this.selectRuleHandler()}
                            onSubmit={() => this.savePolicy()}
                            showError={this.showError}
                          />
                        ) : (
                            <PolicyForm
                              initialValues={this.state.selectedPolicy}
                              classes={classes}
                              onClick={() => this.selectRuleHandler()}
                              onSubmit={() => this.savePolicy()}
                              acl={{ default: "FULL_ACCESS" }}
                            />
                          )}
                      </div>
                    )}
              </React.Fragment>
            ) : this.state.entityStep === 3 ? (
              <React.Fragment>
                <div className={classes.headSection}>
                  <div className={classes.leftNav}>
                    <Button color="warning" onClick={this.goBack}>
                      Back to Policy
                    </Button>
                  </div>
                  <div className={classes.rightNav}>
                    <div className={classes.rightAlign}>
                      {
                        <Button
                          type="submit"
                          color="info"
                          onClick={() => this.props.dispatch(submit('PolicyRule'))}
                        >
                          Save Rule
                        </Button>
                      }
                    </div>
                  </div>
                </div>

                {
                  typeof this.state.selectedRule === "object" &&
                  this.state.selectedRule.ruleId >= 0 &&
                  (
                    this.state.showError !== "" ? (
                      <PolicyRule
                        rulesList={this.state.selectedRule.conditionJson}
                        actionsList={this.state.selectedRule.actionJson}
                        priority={this.state.selectedRule.priority}
                        onPriorityChange={val =>
                          this.updateRuleAction("UPDATE_PRIORITY", null, val)
                        }
                        onConditionAdd={id =>
                          this.updateRuleAction("CONDITION_ADD", id)
                        }
                        onConditionMove={(id, newid) =>
                          this.updateRuleAction("CONDITION_MOVE", id, newid)
                        }
                        onConditionAddGroup={id =>
                          this.updateRuleAction("GROUP_ADD", id)
                        }
                        onConditionRemove={id =>
                          this.updateRuleAction("CONDITION_DELETE", id)
                        }
                        onConditionRemoveGroup={id =>
                          this.updateRuleAction("GROUP_DELETE", id)
                        }
                        onFactChange={(id, val) =>
                          this.updateRuleAction("FACT_CHANGE", id, val)
                        }
                        onActionChange={val =>
                          this.updateRuleAction("ACTION_CHANGE", null, val)
                        }
                        onOpChange={(id, val) =>
                          this.updateRuleAction("OP_CHANGE", id, val)
                        }
                        onSubmit={() => this.saveRule()}
                        classes={classes}
                        acl={this.state.acl.rulesList || this.state.acl.default}
                        showError={this.state.showError}
                      />
                    ) : (
                        <PolicyRule
                          rulesList={this.state.selectedRule.conditionJson}
                          actionsList={this.state.selectedRule.actionJson}
                          priority={this.state.selectedRule.priority}
                          onPriorityChange={val =>
                            this.updateRuleAction(
                              "RULES",
                              "UPDATE_PRIORITY",
                              null,
                              val
                            )
                          }
                          onConditionAdd={id =>
                            this.updateRuleAction("RULES", "CONDITION_ADD", id)
                          }
                          onConditionMove={(id, newid) =>
                            this.updateRuleAction(
                              "RULES",
                              "CONDITION_MOVE",
                              id,
                              newid
                            )
                          }
                          onConditionAddGroup={id =>
                            this.updateRuleAction("RULES", "GROUP_ADD", id)
                          }
                          onConditionRemove={id =>
                            this.updateRuleAction("RULES", "CONDITION_DELETE", id)
                          }
                          onConditionRemoveGroup={id =>
                            this.updateRuleAction("RULES", "GROUP_DELETE", id)
                          }
                          onFactChange={(id, val) =>
                            this.updateRuleAction("RULES", "FACT_CHANGE", id, val)
                          }
                          onActionChange={val =>
                            this.updateRuleAction(
                              "RULES",
                              "ACTION_CHANGE",
                              null,
                              val
                            )
                          }
                          onOpChange={(id, val) =>
                            this.updateRuleAction("RULES", "OP_CHANGE", id, val)
                          }
                          onSubmit={() => this.saveRule()}
                          classes={classes}
                          acl={this.state.acl.rulesList || this.state.acl.default}
                        />
                      )
                  )
                }
              </React.Fragment>
            ) : null}
          </React.Fragment>
        }
      />
    );
  }
}

PolicyList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(policyStyle)(connect()(PolicyList));
