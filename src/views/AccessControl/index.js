import React from "react";
import PolicyList from './PolicyList.jsx'

export default function AccessControl({ ...props }) {
    return (
        <div style={{ minHeight: "600px", padding: 25 }}>
            <PolicyList />
        </div>
    );
}