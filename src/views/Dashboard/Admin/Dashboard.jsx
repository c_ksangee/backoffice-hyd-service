import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
import {
  DateRange,
  LocalOffer,
  Update,
  ArrowUpward,
  AccessTime,
  Receipt,
  MonetizationOn,
  Done
} from "@material-ui/icons";
import { withStyles, Grid, Card, CardHeader } from "@material-ui/core";

import {
  StatsCard,
  ChartCard,
  TasksCard,
  ItemGrid,
  LatestNotifications,
  SiteVisitor
} from "components";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts";

import{
  appNotification,
  announcementsNotification
} from "variables/general";

import dashboardStyle from "assets/jss/material-dashboard-react/dashboardStyle";
import SimpleTagCloud from "components/TagCloud/SimpleTagCloud";

class Dashboard extends React.Component {
  
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    return (
      <div className="testClass3">
      <Grid container>
      <ItemGrid xs={12} sm={12} md={9}>
      <Grid container>
          <ItemGrid xs={12} sm={4} md={4}>
            <StatsCard
              icon={Receipt}
              iconColor="orange"
              title="Order Count"
              description="80,800"
              statIcon={Update}
              statText="Updated 24 hours ago"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={4} md={4}>
            <StatsCard
              icon={MonetizationOn}
              iconColor="green"
              title="Revenue"
              description="$2,33,424"
              statIcon={DateRange}
              statText="Last 24 Hours"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={4} md={4}>
            <StatsCard
              icon={Done}
              iconColor="red"
              title="Approvals Needed"
              description="75"
              statIcon={LocalOffer}
              statText="Tracked from CMS"
            />
          </ItemGrid>
        </Grid>
        <Grid container>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={dailySalesChart.data}
                  type="Line"
                  options={dailySalesChart.options}
                  listener={dailySalesChart.animation}
                />
              }
              chartColor="green"
              title="Daily Sales"
              text={
                <span>
                  <span className={this.props.classes.successText}>
                    <ArrowUpward
                      className={this.props.classes.upArrowCardCategory}
                    />{" "}
                    55%
                  </span>{" "}
                  increase in today sales.
                </span>
              }
              statIcon={AccessTime}
              statText="updated 4 minutes ago"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={emailsSubscriptionChart.data}
                  type="Bar"
                  options={emailsSubscriptionChart.options}
                  responsiveOptions={emailsSubscriptionChart.responsiveOptions}
                  listener={emailsSubscriptionChart.animation}
                />
              }
              chartColor="orange"
              title="Order Stats Month wise"
              text="Future months results(forecast)"
              statIcon={AccessTime}
              statText="updated 4 minutes ago"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={completedTasksChart.data}
                  type="Line"
                  options={completedTasksChart.options}
                  listener={completedTasksChart.animation}
                />
              }
              chartColor="red"
              title="New Customers/ Growth"
              text="Last Campaign Performance"
              statIcon={AccessTime}
              statText="campaign sent 2 days ago"
            />
          </ItemGrid>
        </Grid>
      
      
      </ItemGrid>
      <ItemGrid xs={12} sm={12} md={3}>
      <LatestNotifications appNotification={appNotification}
                                                 announcementsNotification={announcementsNotification}/>
      </ItemGrid>
      </Grid>
       
        <Grid container>
          <ItemGrid xs={12} sm={12} md={9}>
            <TasksCard />
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={3}>
              <div className="tagContainer">
              <SimpleTagCloud/>
              </div>
          </ItemGrid>
        </Grid>
        <Grid container>
        <ItemGrid xs={12} md={12} sm={12}>
        <Card className={"siteVisitorCard"}>
        <CardHeader className={"siteVisitorCardHeader"}
        title={"Site Visitors Statistics"}
        subheader={"Thanks for shopping with us! Love to serve you ! Happy to be partner with your organization ! Below you can find the partner visitor stats to our website ! "}
        />
          <SiteVisitor/>
        </Card>
        </ItemGrid>
        </Grid>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
