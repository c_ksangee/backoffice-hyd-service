import React from "react";
import Admin from './Admin/Dashboard.jsx';
import Florist from './Florist/Dashboard.jsx';
import Partner from './Partner/Dashboard.jsx';

function Content({ ...props }) {
  const role = sessionStorage.getItem('role') || 'user';
  return (
      role.match(/SUPER_ADMIN/) ? <Admin /> : (role.match(/FLORIST/) ? <Florist /> : <Partner />)
  );
}

export default Content;
