import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";

import {
  ArrowUpward,
  AccessTime,
} from "@material-ui/icons";
import { withStyles, Grid } from "@material-ui/core";

import {
  ChartCard,
  RegularCard,
  OrdersCard,
  Table,
  ItemGrid,
  SideProfileCard,
  MyCalendar,
  SingleLineGridList
} from "components";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts";

import dashboardStyle from "assets/jss/material-dashboard-react/dashboardStyle";
import avatar from "assets/img/faces/marc.jpg";
// import MyCalendar from 
// BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment))
class Dashboard extends React.Component {
  
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    return (
      <div className="florist">
      
        <Grid container>
        <ItemGrid xs={12} sm={12} md={12}>
          <SideProfileCard
            avatar={avatar}
            title="Alec Thompson - Member Number # 05-7257AA"
          />
        </ItemGrid>
        </Grid>
        <Grid container>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={dailySalesChart.data}
                  type="Line"
                  options={dailySalesChart.options}
                  listener={dailySalesChart.animation}
                />
              }
              chartColor="green"
              title="Daily Sales"
              text={
                <span>
                  <span className={this.props.classes.successText}>
                    <ArrowUpward
                      className={this.props.classes.upArrowCardCategory}
                    />{" "}
                    55%
                  </span>{" "}
                  increase in today sales.
                </span>
              }
              statIcon={AccessTime}
              statText="updated 4 minutes ago"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={emailsSubscriptionChart.data}
                  type="Bar"
                  options={emailsSubscriptionChart.options}
                  responsiveOptions={emailsSubscriptionChart.responsiveOptions}
                  listener={emailsSubscriptionChart.animation}
                />
              }
              chartColor="orange"
              title="Order Stats Month wise"
              text="Last Campaign Performance"
              statIcon={AccessTime}
              statText="campaign sent 2 days ago"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={4}>
            <ChartCard
              chart={
                <ChartistGraph
                  className="ct-chart"
                  data={completedTasksChart.data}
                  type="Line"
                  options={completedTasksChart.options}
                  listener={completedTasksChart.animation}
                />
              }
              chartColor="red"
              title="On-time Delivery Report"
              text="Last Campaign Performance"
              statIcon={AccessTime}
              statText="campaign sent 2 days ago"
            />
          </ItemGrid>
        </Grid>
        <Grid container>
        <ItemGrid xs={12} sm={12} md={6}>
            <OrdersCard />
          </ItemGrid>
        <ItemGrid xs={12} sm={12} md={6}>
        <RegularCard
              headerColor="green"
              cardTitle="UPCOMING DELIVERY CALENDAR"
              content={
        <MyCalendar />
        }
            />
        </ItemGrid>
        </Grid>
        <Grid container>
        <ItemGrid xs={12} sm={12} md={12}>
        <RegularCard
              headerColor="orange"
              cardTitle="Best Selling Flowers"
              content={
                <SingleLineGridList />
              }
              />
        </ItemGrid>
        </Grid>
        <Grid container>
          <ItemGrid xs={12} sm={12} md={12}>
            <RegularCard
               headerColor="green"
              cardTitle="Best Florists in FTD Network"
              cardSubtitle="As per Last Campaign Performance"
              content={
                <Table
                  tableHeaderColor="warning"
                  tableHead={["ID", "Name", "Orders Delivered", "State"]}
                  tableData={[
                    ["1", "John Michels", "558", "New York"],
                    ["2", "Richards", "456", "IL"],
                    ["3", "Sage", "359", "California"],
                    ["4", "Philip", "350", "Alaska"]
                  ]}
                />
              }
            />
          </ItemGrid>
        </Grid>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
