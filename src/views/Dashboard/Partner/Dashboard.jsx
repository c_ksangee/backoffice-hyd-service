import React from "react";
import PropTypes from "prop-types";
import {
  DateRange,
  LocalOffer,
  Update,
  Accessibility,
  Receipt,
  AccountBalanceWallet,
  Loyalty
} from "@material-ui/icons";
import { withStyles, Grid, Card, CardHeader } from "@material-ui/core";

import {
  StatsCard,
  RegularCard,
  ItemGrid,
  SiteVisitor,
  SingleLineGridList
} from "components";

import {expanseData} from "variables/general";

import dashboardStyle from "assets/jss/material-dashboard-react/dashboardStyle";
import PointsRevenue from "components/PointsRevenue/PointsRevenue";
class Dashboard extends React.Component {
  
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    return (
      <div className="testClass1">
      
        <Grid container>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Receipt}
              iconColor="orange"
              title="Partner Orders"
              description="550"
              small="/day"
              statIcon={Update}
              statText="Just Updated"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={AccountBalanceWallet}
              iconColor="green"
              title="Miles/Points"
              description="775"
              statIcon={LocalOffer}
              statText="Tracked from Partner"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Loyalty}
              iconColor="red"
              title="Redeemed Miles/Points"
              description="220"
              statIcon={DateRange}
              statText="Provided to Partner"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Accessibility}
              iconColor="blue"
              title="Followers(Partners)"
              description="+245"
              statIcon={Update}
              statText="Just Updated"
            />
          </ItemGrid>
        </Grid>
        <Grid container>
          <ItemGrid xs={12} sm={12} md={6}>
          <Card className={"pointsRevenueCard"}>
        <CardHeader className={"pointsRevenueCardHeader"}
        title={"User Miles/Points Revenue Statstics"}
        subheader={"Miles/Points info provided by your organization. You can redeem you miles/points with us. We made our products with love. Thank you !"}
        />
            <PointsRevenue chartData={expanseData}/>
            </Card>
          </ItemGrid>
          <ItemGrid xs={12} sm={12} md={6}>
          <Card className={"siteVisitorCard"}>
          <CardHeader className={"siteVisitorCardHeader"}
          title={"Site Visitors(partners) Statistics"}
          subheader={"Thanks for shopping with us! Love to serve you ! Happy to be partner with your organization ! Below you can find the partner visitor stats to our website ! "}
          />
          <SiteVisitor/>
        </Card>
          </ItemGrid>
        </Grid>
        <Grid container>
          <ItemGrid xs={12} sm={12} md={12}>
            <RegularCard
              headerColor="orange"
              cardTitle="Special flowers especially for partners with attractive discounts"
              cardSubtitle="Upgraded catalog with utmost care for you !"
              content={
                <SingleLineGridList />
              }
            />
          </ItemGrid>
        </Grid>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
