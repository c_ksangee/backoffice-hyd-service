import React from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import renderField from "library/actions/renderField.jsx";
import {
  CircularProgress,
  Table,
  TableCell,
  TableRow,
  TableBody,
  Grid,
} from "@material-ui/core";
import { PaginationTable, Muted } from "components";
import ruleFieldsList from "./DeliveryFeesEntity.json";

import {
  ConditionManager,
  ItemGrid
} from "components";

const style = {
  error: {
    textAlign: "center",
    color: "red"
  }
};

const columnList = [
  {
    id: "ruleId",
    numeric: true,
    label: "Rule ID"
  },
  {
    id: "priority",
    numeric: true,
    label: "Priority"
  },
  {
    id: "conditionJson",
    numeric: false,
    label: "Conditions",
    style: { width: "100px" }
  },
  {
    id: "actionJson",
    numeric: false,
    label: "Action",
    style: { width: "70px" }
  }
];

class DeliveryFeesForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      feeSubmitInProgress: false
    };
  }

  doSubmit() {
    this.setState({ feeSubmitInProgress: true });

    this.props.handleSubmit().then(resolve => {
      this.setState({ feeSubmitInProgress: false });
    });
  }

  render() {
    const { classes, initialValues, acl, showError } = this.props;

    return (

      <div className={`${classes.leftAlign}`}>
        {typeof initialValues === "object" ? (
          <div>
            {
              <div>
                {showError !== "" && <div style={style.error}><h4>{showError}</h4></div>}

                {this.state.feeSubmitInProgress === true &&
                  <div>
                    <CircularProgress
                      size={40}
                      style={{ color: "rgb(202, 173, 89)" }}
                    />
                  </div>
                }
              </div>
            }
            <form>
              <div className={`${classes.custTable}`}>
                <div className={`${classes.custTableBody}`}>
                  <div className={`${classes.custTableRow}`}>
                    <div>
                      <Grid container>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="label"
                            classes={classes}
                            label="ID:"
                            name="feeId"
                            default="<Auto Generated>"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Name:"
                            name="feeName"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Description:"
                            name="feeDesc"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Type:"
                            name="feeType"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Code:"
                            name="feeCode"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="text"
                            classes={classes}
                            label="Channel:"
                            name="channel"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="selectfield"
                            classes={classes}
                            label="Site:"
                            name="siteId"
                            options={["ftd", "proflowers"]}
                            mode="horizontal"
                          />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                          <Field displayType='material-ui'
                            acl={acl}
                            component={renderField}
                            render_type="selectfield"
                            classes={classes}
                            label="Status:"
                            name="status"
                            options={["active", "inactive"]}
                            mode="horizontal"
                          />
                        </ItemGrid>
                      </Grid>
                    </div>
                  </div>
                </div>
              </div>

              <Table>
                <TableBody>
                  {initialValues.surchargeJson !== null &&
                    typeof initialValues.surchargeJson === "object" && (
                      <TableRow>
                        <TableCell colSpan="2">
                          <h3>Surcharge Rules</h3>
                          <ConditionManager
                            rulesList={initialValues.surchargeJson}
                            fieldsList={ruleFieldsList}
                            onConditionAdd={this.props.onConditionAdd}
                            onConditionMove={this.props.onConditionMove}
                            onConditionAddGroup={this.props.onConditionAddGroup}
                            onConditionRemove={this.props.onConditionRemove}
                            onConditionRemoveGroup={
                              this.props.onConditionRemoveGroup
                            }
                            onFactChange={this.props.onFactChange}
                            onOpChange={this.props.onOpChange}
                            classes={classes}
                            readOnly={                              
                                typeof acl.surchargeJson !== "undefined"
                                ? acl.surchargeJson !== "FULL_ACCESS"
                                : acl.default !== "FULL_ACCESS"
                            }
                          />
                        </TableCell>
                      </TableRow>
                    )}
                  {
                    ((typeof acl.rulesList !== "undefined")
                      ? acl.rulesList !== "NO_ACCESS"
                      : acl.default !== "NO_ACCESS") &&

                    <TableRow>
                      <TableCell colSpan="2">
                        <h3>Condition Rules</h3>
                        <Muted>
                          Click on a rule below to edit
                      </Muted>
                        {typeof initialValues.rulesList === "object" &&
                          initialValues.rulesList.length > 0 && (
                            <PaginationTable
                              columnData={columnList}
                              bodyData={initialValues.rulesList}
                              onhandleClickCallback={(id, type) =>
                                this.props.onClick(id)
                              }
                              keyColumn="ruleId"
                              orderBy="ruleId"
                              order="asc"
                            />
                          )}
                      </TableCell>
                    </TableRow>
                  }
                </TableBody>
              </Table>
            </form>
          </div>
        ) : (
            <p>Unable to fetch fee details. Please try after some time</p>
          )}
      </div>
    );
  }
}

DeliveryFeesForm = reduxForm({
  form: "DeliveryFeesForm"
})(DeliveryFeesForm);

DeliveryFeesForm.propTypes = {
  classes: PropTypes.object.isRequired
};

export default DeliveryFeesForm;
