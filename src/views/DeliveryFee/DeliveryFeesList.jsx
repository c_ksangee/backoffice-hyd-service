import React from "react";
import PropTypes from "prop-types";

import {
  withStyles,
  CircularProgress,
  Chip,
  Avatar,
  Grid
} from "@material-ui/core";

import {
  Button,
  PaginationTable,
  ItemGrid,
  Muted,
  RegularCard
} from "components";

import DeliveryFeesForm from "./DeliveryFeesForm.jsx";
import DeliveryFeesRule from "./DeliveryFeesRule.jsx";
import { connect } from 'react-redux';
import { submit } from 'redux-form'

//import { getFeesListBySite, createFees, updateFees, getFeesById, deleteFeesById } from 'library/actions/deliveryfees.jsx';
import {
  getFeesListBySite,
  getFeesByIdMock,
  updateFees
} from "library/actions/deliveryfees.jsx";
import { getPolicySet, getActionSet } from "library/actions/policy.jsx";
import RulesEngine from "components/Rules/RulesEngine.js";
import deliveryFeesStyle from "assets/jss/material-dashboard-react/deliveryFeesStyle.jsx";

const siteNameMap = {
  FTD: "FTD",
  PROFLOWERS: "PRO",
  INTERFLORA: "IF"
};

const columnList = [
  {
    id: "feeId",
    numeric: true,
    label: "Fee ID"
  },
  {
    id: "feeName",
    numeric: false,
    label: "Name"
  },
  {
    id: "feeCode",
    numeric: false,
    label: "Code"
  },
  {
    id: "feeType",
    numeric: false,
    label: "Type"
  },
  {
    id: "channel",
    numeric: false,
    label: "Channel"
  },
  {
    id: "status",
    numeric: false,
    label: "Status"
  }
];

class DeliveryFeesList extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.initialState();
  }

  initialState(override) {

    return {
      entityStep: 1,
      siteId: siteNameMap[sessionStorage.getItem("site")],
      role: sessionStorage.getItem("role"),
      acl: {
        default: "NO_ACCESS"
      },
      title: "List of Delivery Fees",
      search: {
        channel: "0",
        feeCode: {
          ftd: "AlaskaHawaii",
          proflowers: "Care & Handling",
          interflora: "Same Day Delivery"
        }
      },
      showFeesList: false,
      feesList: [],
      showMessage: "",
      showError: "",
      selectedFee: "",
      feesData: {},
      showFeeDetails: false,
      ...override
    };
  }

  titleSetter = entityStep => {
    switch (entityStep) {
      case 1:
        return "List of Delivery Charges";
      case 2:
        return "Create or Update Delivery Charge";
      case 3:
        return "Edit Delivery Charge Rule Condition";
      default:
        return "List of Delivery Charges";
    }
  };

  fetchFeesHandler = feeId => {
    if (this.state.acl === "") {
      this.setState({
        showFeeDetails: true,
        showMessage:
          "You are not authorised to see the fee details. Please check with Administrator."
      });
    } else {
      this.setState({ selectedFee: feeId, title: "Update Delivery Fee" }, () => {
        //getFeesById(this.state.siteId, this.state.selectedFee)
        getFeesByIdMock(this.state.feesList, this.state.selectedFee) //mock: replace this line with above
          .then(response => {
            //if (response.status === 200) {
            //  response.json().then((feesData) => {
            if (typeof response === "object") {
              //mock: replace this line with above
              const feesData = response;       //mock: replace this line with above

              if (typeof this.state.customRules === "object") {
                let actionSetId = RulesEngine.RunEngine(
                  feesData,
                  this.state.customRules
                );

                if (
                  actionSetId !== "FULL_ACCESS" &&
                  actionSetId !== "READ_ONLY" &&
                  actionSetId !== "NO_ACCESS"
                ) {
                  this.setState({ actionSetId, feesData }, this.fetchActionSet);
                } else {
                  this.setState({
                    showFeeDetails: true,
                    feesData,
                    acl: {
                      ...this.state.acl,
                      default: actionSetId
                    },
                    showMessage: "",
                    entityStep: 2
                  });
                }
              } else {
                if (
                  this.state.acl.default !== "FULL_ACCESS" &&
                  this.state.acl.default !== "READ_ONLY" &&
                  this.state.acl.default !== "NO_ACCESS"
                ) {
                  this.setState(
                    { actionSetId: this.state.acl.default, feesData },
                    this.fetchActionSet
                  );
                } else {
                  this.setState({
                    showFeeDetails: true,
                    feesData: feesData,
                    showMessage: "",
                    entityStep: 2
                  });
                }
              }
              //  });
            } else {
              this.setState({
                showFeeDetails: true,
                showMessage:
                  "Unable to fetch fee details. Please try after sometime."
              });
            }
          })
          .catch(err => {
            this.setState({
              showFeeDetails: true,
              showMessage:
                "Unable to fetch fee details due to an unexpected error. Please try after sometime."
            });
          });
      });
    }
  };

  fetchPolicySet() {
    if (this.state.role === "" || this.state.siteId === "") return;

    getPolicySet(this.state.siteId, "fee", this.state.role).then(
      response => {
        if (response.status === 200) {
          response.json().then(resobj => {
            if (
              resobj["policySet"] !== null &&
              typeof resobj["policySet"] !== "undefined"
            ) {
              const policySet =
                typeof resobj["policySet"]["policy"] !== "undefined"
                  ? resobj["policySet"]["policy"]
                  : {};

              if (policySet.action === "NO_ACCESS") {
                this.setState({
                  showFeesList: true,
                  showMessage:
                    "You are not authorized to see this page. Please check with Administrator."
                });
              } else {
                this.setState(
                  {
                    customRules: policySet.customRules,
                    acl: {
                      ...this.state.acl,
                      default: policySet.action
                    }
                  },
                  this.fetchFeesList
                );
              }

            } else {
              this.setState({
                showFeesList: true,
                showMessage:
                  "You are not authorized to see this page. Please check with Administrator."
              });
            }
          });
        } else {
          this.setState({
            showFeesList: true,
            showMessage:
              "Unable to fetch policy set. Please try after sometime."
          });
        }
      },
      reject => {
        this.setState({
          showFeesList: true,
          showMessage:
            "Unable to fetch policy set due to an unexpected error. Please try after sometime."
        });
      }
    );
  }

  fetchActionSet() {
    if (this.state.actionSetId === "") {
      this.setState({
        showFeesList: true,
        showMessage:
          "You are not authorized to see this page. Please check with Administrator."
      });
      return;
    }

    getActionSet("fee", this.state.actionSetId).then(
      response => {
        if (response.status === 200) {
          response.json().then(res => {
            let acl = {};
            if (typeof res === "object" && typeof res.actionSet === "object") {
              if (typeof res.actionSet.attributeSet === "object") {
                acl = res.actionSet.attributeSet.reduce((obj, item) => {
                  obj[item.name] = item.action;
                  return obj;
                }, {});
              }
              acl.default = res.actionSet.defaultAction;
            } else {
              acl.default = "NO_ACCESS";
            }

            this.setState({
              showFeeDetails: true,
              acl,
              showMessage: "",
              entityStep: 2
            });
          });
        } else {
          this.setState({
            showFeeDetails: true,
            showMessage:
              "Unable to fetch policy action set. Please try after sometime."
          });
        }
      },
      reject => {
        this.setState({
          showFeeDetails: true,
          showMessage:
            "Unable to fetch policy action set due to an unexpected error. Please try after sometime."
        });
      }
    );
  }

  fetchFeesList() {
    if (this.state.siteId === "") return;

    getFeesListBySite(this.state.siteId).then(
      response => {
        if (response.status === 200) {
          response.json().then(list => {
            this.setState({
              showFeesList: true,
              feesList: list[this.state.siteId],
              showMessage: ""
            });
          });
        } else {
          this.setState({
            showFeesList: true,
            showMessage:
              "Unable to fetch delivery fees list. Please try after sometime."
          });
        }
      },
      reject => {
        this.setState({
          showFeesList: true,
          showMessage:
            "Unable to fetch delivery fees list due to an unexpected error. Please try after sometime."
        });
      }
    );
  }

  createFeesHandler = feeId => {
    this.setState({
      entityStep: 2,
      showMessage: "",
      showError: "",
      showFeeDetails: true,
      title: "Create Delivery Fee",
      feesData: {}
    });
  };

  goBack = event => {
    this.setState({
      entityStep: this.state.entityStep - 1,
      title: this.titleSetter(this.state.entityStep - 1)
    });
  };

  saveFee(values) {
    values.feesData = this.state.feesData;
    return updateFees(this.state.siteId, values).then(
      response => {
        if (response.status === 200) {
          this.setState({
            entityStep: 1,
            showMessage:
              "Updated fees #" +
              this.state.selectedFee +
              " details successfully."
          });
        } else {
          this.setState({
            showError:
              "Unable to update fee #" + this.state.selectedFee + " details."
          });
        }
      },
      reject => {
        this.setState({
          showError:
            "Unable to update fee #" + this.state.selectedFee + " details."
        });
      }
    );
  }

  selectRuleHandler = ruleId => {
    let ruleIndex;
    const rule = this.state.feesData.rulesList.find((v, i) => {
      if (v.ruleId === ruleId) {
        ruleIndex = i;
        return v;
      }
    });

    this.setState({
      entityStep: 3,
      selectedRule: rule,
      title: this.titleSetter(3),
      selectedRuleIndex: ruleIndex
    });
  };

  saveRule = () => {

    const newFeesData = {
      ...this.state.feesData
    };

    newFeesData.rulesList[
      this.state.selectedRuleIndex
    ] = this.state.selectedRule;

    this.setState({
      feesData: newFeesData
    });

    return updateFees(this.state.siteId, this.state.feesData).then(
      response => {
        if (response.status === 200) {
          this.setState({
            entityStep: 1,
            showMessage:
              "Updated fees #" +
              this.state.selectedFee +
              " details successfully."
          });
        } else {
          this.setState({ showError: "Unable to update fee rule details." });
        }
      },
      reject => {
        this.setState({ showError: "Unable to update fee rule details." });
      }
    );
  };

  updateRuleAction = (source, type, id, newVal) => {
    if (id === ".") id = "";

    let newRule =
      source === "SURCHARGE"
        ? this.state.feesData.surchargeJson
        : this.state.selectedRule.conditionJson;
    let newRuleVal = {};

    switch (type) {
      case "GROUP_ADD":
        eval(`newRule${id}.push({'all':[]})`);
        let len = eval(`newRule${id}.length - 1`);
        id = id + "[" + len + "].all";
        eval(`newRule${id}.push({entity:'',operator:'', value:'', field:''})`);
        break;
      case "GROUP_DELETE":
        let newGIdArr = id
          .match(/(.*)\.([^(\d+)]*$)/)[1]
          .match(/(.*)\[(\d+)]$/);
        eval(`newRule${newGIdArr[1]}.splice(newGIdArr[2],1)`);
        break;
      case "CONDITION_ADD":
        eval(`newRule${id}.push({entity:'',operator:'', value:'', field:''})`);
        break;
      case "CONDITION_MOVE":
        let newDArr = newVal.match(/(.*)\[(\d+)]$/);
        let newSArr = id.match(/(.*)\[(\d+)]$/);

        if (newDArr[1] === newSArr[1] && newDArr[2] < newSArr[2]) {
          // delete source and add destination
          eval(`newRule${newSArr[1]}.splice(newSArr[2], 1)`);
          eval(`newRule${newDArr[1]}.splice(newDArr[2], 0, rule)`);
        } else {
          //add to destination and delete destination
          eval(`newRule${newDArr[1]}.splice(newDArr[2], 0, rule)`);
          eval(`newRule${newSArr[1]}.splice(newSArr[2], 1)`);
        }
        break;
      case "CONDITION_DELETE":
        let newArr = id.match(/(.*)\[(\d+)]$/);
        if (eval(`newRule${newArr[1]}.length === 1`)) {
          let newCIdArr = newArr[1]
            .match(/(.*)\.([^(\d+)]*$)/)[1]
            .match(/(.*)\[(\d+)]$/);
          eval(`newRule${newCIdArr[1]}.splice(newCIdArr[2],1)`);
        } else {
          eval(`newRule${newArr[1]}.splice(newArr[2],1)`);
        }
        break;
      case "FACT_CHANGE":
        eval(`newRule${id} = newVal`);
        break;
      case "OP_CHANGE":
        const oldop = newVal === "all" ? "any" : "all";
        eval(
          `newRule${id}.${newVal} = newRule${id}.${oldop}; delete newRule${id}.${oldop}`
        );
        break;
      case "UPDATE_PRIORITY":
        newRuleVal.priority = newVal;
        break;
      case "ACTION_CHANGE":
        newRuleVal.actionJson = newVal;
        break;
      default:
        eval();
    }

    if (source === "SURCHARGE") {
      this.setState(prevState => ({
        ...prevState,
        feesData: {
          ...prevState.feesData,
          surchargeJson: newRule
        }
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        selectedRule: {
          ...prevState.selectedRule,
          conditionJson: newRule,
          ...newRuleVal
        }
      }));
    }
  };

  componentWillMount() {
    this.fetchPolicySet();
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container>
        <ItemGrid xs={12} sm={12} md={12}>
          <RegularCard headerColor="green"
            cardTitle={this.state.title}
            content={
              <React.Fragment>
                {this.state.entityStep === 1 ? (
                  <React.Fragment>
                    <Grid container>
                      <ItemGrid xs={12} sm={12} md={12}>
                        <div className={classes.centerAlign}>
                          {
                            this.state.acl.default !== "NO_ACCESS" &&
                            <div className={classes.headSection}>
                              <div className={classes.leftNav}>
                                <div className={classes.titleSection}>
                                  <Muted>
                                    Select a delivery fee entry from the below to edit
                                  </Muted>
                                </div>
                              </div>
                              <div className={classes.rightNav}>
                                <div className="buttonSection">
                                  {this.state.acl.default === "FULL_ACCESS" && (
                                    <div className={classes.rightAlign}>
                                      <Button onClick={this.createFeesHandler} color="info">
                                        Create Delivery Fee
                                      </Button>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>
                          }
                          <div className={classes.clear} />
                          <div style={{ border: '1px solid', margin: '10px 0px' }}>
                            {
                              this.state.siteId !== "" &&
                              this.state.role !== "" &&
                              (
                                this.state.showFeesList === false ? (
                                  <div className={classes.marginTop}>
                                    <CircularProgress size={60} />
                                  </div>
                                )
                                  : this.state.feesList.length === 0 ? (
                                    <div>
                                      <Chip
                                        avatar={<Avatar />}
                                        label={`There are no delivery fees configured for ${
                                          this.state.siteId
                                          } site.`}
                                        className={classes.chip}
                                      />
                                    </div>
                                  )
                                    :
                                    <div>
                                      {
                                        this.state.showMessage !== "" &&
                                        <Chip
                                          avatar={<Avatar />}
                                          label={this.state.showMessage}
                                          className={classes.chip}
                                        />
                                      }
                                      <div>
                                        <PaginationTable
                                          columnData={columnList}
                                          bodyData={this.state.feesList}
                                          onhandleClickCallback={this.fetchFeesHandler}
                                          keyColumn="feeId"
                                          className="testClass"
                                        />
                                      </div>
                                    </div>
                              )}
                          </div>
                        </div>
                      </ItemGrid>
                    </Grid>
                  </React.Fragment>
                ) : this.state.entityStep === 2 ? (
                  <React.Fragment>
                    <Grid container>
                      <ItemGrid xs={12} sm={12} md={12}>
                        <div className={classes.headSection}>
                          <div className={classes.leftNav}>
                            <div>
                              <Button color="warning" onClick={this.goBack}>
                                Back to List
                              </Button>
                            </div>
                          </div>
                          <div className={classes.rightNav}>
                            {
                              <div>
                                {
                                  (this.state.feeSubmitInProgress === true) ?
                                    <div>
                                      <CircularProgress size={40}
                                        style={{ color: "rgb(202, 173, 89)" }}
                                      />
                                    </div>
                                    :
                                    <div className={classes.rightAlign}>
                                      {
                                        Object.keys(this.state.acl).filter(key => this.state.acl[key] === 'FULL_ACCESS').length > 0 &&
                                        <Button type="submit" color="info" onClick={() => this.props.dispatch(submit('DeliveryFeesForm'))}>Save Fee</Button>
                                      }
                                    </div>
                                }
                              </div>
                            }
                          </div>
                        </div>
                      </ItemGrid>

                      <ItemGrid xs={12} sm={12} md={12} style={{ border: '1px solid' }}>
                        {this.state.showFeeDetails === false ? (
                          <div className={`${classes.marginTop} ${classes.centerAlign}`}>
                            <CircularProgress
                              className={classes.refreshLoading}
                              size={60}
                            />
                          </div>
                        ) : this.state.showMessage !== "" ? (
                          <div className={`${classes.marginTop} ${classes.centerAlign}`}>
                            <Chip
                              avatar={<Avatar />}
                              label={this.state.showMessage}
                              className={classes.chip}
                            />
                          </div>
                        ) : (
                              <div>
                                {this.state.acl.default !== "NO_ACCESS" &&
                                  this.state.showError !== "" ? (
                                    <DeliveryFeesForm
                                      initialValues={this.state.feesData}
                                      acl={this.state.acl}
                                      classes={classes}
                                      onClick={this.selectRuleHandler}
                                      onSubmit={values => this.saveFee(values)}
                                      showError={this.state.showError}
                                      onPriorityChange={val =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "UPDATE_PRIORITY",
                                          null,
                                          val
                                        )
                                      }
                                      onConditionAdd={id =>
                                        this.updateRuleAction("SURCHARGE", "CONDITION_ADD", id)
                                      }
                                      onConditionMove={(id, newid) =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "CONDITION_MOVE",
                                          id,
                                          newid
                                        )
                                      }
                                      onConditionAddGroup={id =>
                                        this.updateRuleAction("SURCHARGE", "GROUP_ADD", id)
                                      }
                                      onConditionRemove={id =>
                                        this.updateRuleAction("SURCHARGE", "CONDITION_DELETE", id)
                                      }
                                      onConditionRemoveGroup={id =>
                                        this.updateRuleAction("SURCHARGE", "GROUP_DELETE", id)
                                      }
                                      onFactChange={(id, val) =>
                                        this.updateRuleAction("SURCHARGE", "FACT_CHANGE", id, val)
                                      }
                                      onActionChange={val =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "ACTION_CHANGE",
                                          null,
                                          val
                                        )
                                      }
                                      onOpChange={(id, val) =>
                                        this.updateRuleAction("SURCHARGE", "OP_CHANGE", id, val)
                                      }
                                    />
                                  ) : (
                                    <DeliveryFeesForm
                                      initialValues={this.state.feesData}
                                      acl={this.state.acl}
                                      classes={classes}
                                      onClick={this.selectRuleHandler}
                                      onSubmit={values => this.saveFee(values)}
                                      onPriorityChange={val =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "UPDATE_PRIORITY",
                                          null,
                                          val
                                        )
                                      }
                                      onConditionAdd={id =>
                                        this.updateRuleAction("SURCHARGE", "CONDITION_ADD", id)
                                      }
                                      onConditionMove={(id, newid) =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "CONDITION_MOVE",
                                          id,
                                          newid
                                        )
                                      }
                                      onConditionAddGroup={id =>
                                        this.updateRuleAction("SURCHARGE", "GROUP_ADD", id)
                                      }
                                      onConditionRemove={id =>
                                        this.updateRuleAction("SURCHARGE", "CONDITION_DELETE", id)
                                      }
                                      onConditionRemoveGroup={id =>
                                        this.updateRuleAction("SURCHARGE", "GROUP_DELETE", id)
                                      }
                                      onFactChange={(id, val) =>
                                        this.updateRuleAction("SURCHARGE", "FACT_CHANGE", id, val)
                                      }
                                      onActionChange={val =>
                                        this.updateRuleAction(
                                          "SURCHARGE",
                                          "ACTION_CHANGE",
                                          null,
                                          val
                                        )
                                      }
                                      onOpChange={(id, val) =>
                                        this.updateRuleAction("SURCHARGE", "OP_CHANGE", id, val)
                                      }
                                    />
                                  )}
                              </div>
                            )}
                      </ItemGrid>
                    </Grid>
                  </React.Fragment>
                ) : this.state.entityStep === 3 ? (
                  <React.Fragment>
                    <div className={classes.headSection}>
                      <div className={classes.leftNav}>
                        <Button color="warning" onClick={this.goBack}>
                          Back to Fee
                        </Button>
                      </div>
                      <div className={classes.rightNav}>
                        <div className={classes.rightAlign}>
                          {
                            (this.state.acl.rulesList === 'FULL_ACCESS' ||
                              (typeof this.state.acl.rulesList === 'undefined' && this.state.acl.default === 'FULL_ACCESS')
                            ) &&
                            <Button type="submit" color="info" onClick={() => this.props.dispatch(submit('DeliveryFeesRule'))}>Save Rule</Button>
                          }
                        </div>
                      </div>
                    </div>

                    <div  style={{ border: '1px solid', margin: '10px 0px' }}>
                    {
                      typeof this.state.selectedRule === "object" &&
                      this.state.selectedRule.ruleId >= 0 &&
                      (
                        this.state.showError !== "" ? (
                          <DeliveryFeesRule
                            rulesList={this.state.selectedRule.conditionJson}
                            actionsList={this.state.selectedRule.actionJson}
                            priority={this.state.selectedRule.priority}
                            onPriorityChange={val =>
                              this.updateRuleAction("RULES", "UPDATE_PRIORITY", null, val)
                            }
                            onConditionAdd={id =>
                              this.updateRuleAction("RULES", "CONDITION_ADD", id)
                            }
                            onConditionMove={(id, newid) =>
                              this.updateRuleAction("RULES", "CONDITION_MOVE", id, newid)
                            }
                            onConditionAddGroup={id =>
                              this.updateRuleAction("RULES", "GROUP_ADD", id)
                            }
                            onConditionRemove={id =>
                              this.updateRuleAction("RULES", "CONDITION_DELETE", id)
                            }
                            onConditionRemoveGroup={id =>
                              this.updateRuleAction("RULES", "GROUP_DELETE", id)
                            }
                            onFactChange={(id, val) =>
                              this.updateRuleAction("RULES", "FACT_CHANGE", id, val)
                            }
                            onActionChange={val =>
                              this.updateRuleAction("RULES", "ACTION_CHANGE", null, val)
                            }
                            onOpChange={(id, val) =>
                              this.updateRuleAction("RULES", "OP_CHANGE", id, val)
                            }
                            onSubmit={() => this.saveRule()}
                            classes={classes}
                            acl={this.state.acl.rulesList || this.state.acl.default}
                            showError={this.state.showError}
                          />
                        ) : (
                            <DeliveryFeesRule
                              rulesList={this.state.selectedRule.conditionJson}
                              actionsList={this.state.selectedRule.actionJson}
                              priority={this.state.selectedRule.priority}
                              onPriorityChange={val =>
                                this.updateRuleAction("RULES", "UPDATE_PRIORITY", null, val)
                              }
                              onConditionAdd={id =>
                                this.updateRuleAction("RULES", "CONDITION_ADD", id)
                              }
                              onConditionMove={(id, newid) =>
                                this.updateRuleAction("RULES", "CONDITION_MOVE", id, newid)
                              }
                              onConditionAddGroup={id =>
                                this.updateRuleAction("RULES", "GROUP_ADD", id)
                              }
                              onConditionRemove={id =>
                                this.updateRuleAction("RULES", "CONDITION_DELETE", id)
                              }
                              onConditionRemoveGroup={id =>
                                this.updateRuleAction("RULES", "GROUP_DELETE", id)
                              }
                              onFactChange={(id, val) =>
                                this.updateRuleAction("RULES", "FACT_CHANGE", id, val)
                              }
                              onActionChange={val =>
                                this.updateRuleAction("RULES", "ACTION_CHANGE", null, val)
                              }
                              onOpChange={(id, val) =>
                                this.updateRuleAction("RULES", "OP_CHANGE", id, val)
                              }
                              onSubmit={() => this.saveRule()}
                              classes={classes}
                              acl={this.state.acl.rulesList || this.state.acl.default}
                            />
                          )
                      )
                      }
                      </div>
                  </React.Fragment>
                ) : null}
              </React.Fragment>
            }
          />
        </ItemGrid>
      </Grid>
    );
  }
}

DeliveryFeesList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(deliveryFeesStyle)(connect()(DeliveryFeesList));
