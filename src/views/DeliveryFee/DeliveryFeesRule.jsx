import React from "react";
import PropTypes from "prop-types";
import { reduxForm } from 'redux-form';

import { ConditionManager, ActionManager } from "components";
import { CircularProgress } from '@material-ui/core';
import ruleFieldsList from './DeliveryFeesEntity.json';

const style = {
    error: {
        textAlign: 'center',
        color: 'red'
    }
};

class DeliveryFeesRule extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            submitStatus: 'initial'
        }
    }

    doSubmit() {
        this.setState({ submitStatus: 'started' });

        this.props.handleSubmit()
            .then((resolve) => {
                this.setState({ submitStatus: 'complete' });
            });
    }

    render() {

        const { classes, actionsList, rulesList, priority, onPriorityChange, onConditionAdd, onConditionMove, onConditionAddGroup, onConditionRemove, onConditionRemoveGroup, onFactChange, onActionChange, onOpChange, showError, acl } = this.props;
        
        return (
            (acl !== 'NO_ACCESS') &&
            <div className={classes.editRuleContainer}>
                <div>
                    {(showError !== '') && <div style={style.error}><h4>{showError}</h4></div>}
                    {
                        (this.state.submitStatus === 'started') &&
                            <div>
                                <CircularProgress size={40}
                                    style={{ color: "rgb(202, 173, 89)" }}
                                />
                            </div>
                        
                    }
                </div>
                <h3 className={classes.pageSectionTitle}>Conditions</h3>
                <ConditionManager
                    rulesList={rulesList}
                    fieldsList={ruleFieldsList}
                    onConditionAdd={onConditionAdd}
                    onConditionMove={onConditionMove}
                    onConditionAddGroup={onConditionAddGroup}
                    onConditionRemove={onConditionRemove}
                    onConditionRemoveGroup={onConditionRemoveGroup}
                    onFactChange={onFactChange}
                    onOpChange={onOpChange}
                    readOnly={(acl !== 'FULL_ACCESS')}
                />

                <h3 className={classes.pageSectionTitle}>Action</h3>
                <ActionManager
                    actionsList={actionsList}
                    onActionChange={onActionChange}
                    readOnly={(acl !== 'FULL_ACCESS')}
                />

                <h3 className={classes.pageSectionTitle}>Priority</h3>
                <div className={classes.prioritySection}>
                    <div> Set priority for this Rule : <input className={classes.priority} type="text" disabled={(acl !== 'FULL_ACCESS')} value={priority} onChange={(e) => onPriorityChange(e.target.value)} /> </div>
                </div>
               
            </div>
        )
    }
}

DeliveryFeesRule = reduxForm({
    form: 'DeliveryFeesRule'
})(DeliveryFeesRule);

DeliveryFeesRule.propTypes = {
    classes: PropTypes.object.isRequired
};

export default DeliveryFeesRule;
