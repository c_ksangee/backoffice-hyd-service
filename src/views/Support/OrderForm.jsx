import React from "react";
import PropTypes from "prop-types";

import { reduxForm, FormSection, Field } from 'redux-form';
import { Button } from "components";
import { Table, TableCell, TableRow, CircularProgress, IconButton } from '@material-ui/core';
import { DeviceHub } from '@material-ui/icons';

import renderField from "library/actions/renderField.jsx";
import services from 'library/api/services';
import validate from './ValidateForm.jsx';
import isPromise from 'is-promise';

const style = {
    error: {
        textAlign: 'center',
        color: 'red'
    }
};

class OrderForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orderSubmitInProgress: false
        }
    }

    doSubmit(e) {

        this.setState({ orderSubmitInProgress: true });

        var result = this.props.handleSubmit();

        if (isPromise(result)) {
            result.then((resolve) => {
                this.setState({ orderSubmitInProgress: false });
            });
        } else {
            this.setState({ orderSubmitInProgress: false });
            e.preventDefault();
            e.stopPropagation();
        }
    }

    render() {
        const { classes, initialValues, showError, acl } = this.props;

        return (
            <div className={`${classes.leftAlign} ${classes.marginTop}`}>
                {
                    (typeof initialValues === 'object') ?
                        <div>
                            <h3 className={classes.centerAlign} style={{ backgroundColor: "#00ACC1", color: "#FFF" }}>ORDER #{initialValues.orderId}</h3>
                            <div className={classes.rightAlign}>
                                <Table style={{ width: "80%" }}>
                                    <TableRow>
                                        <TableCell><h6 style={{ margin: 0, paddingBottom: 10, color: "#00ACC1" }}>WorkFlow:</h6></TableCell>
                                        <TableCell>
                                            <h6 style={{ margin: 0, paddingBottom: 10 }}>{initialValues.workflowId}</h6>
                                        </TableCell>
                                        <TableCell>
                                            <h6 style={{ margin: 0, paddingBottom: 10 }}>
                                                <a href="#workflow">Click to see workflow</a>
                                                <IconButton href="#workflow" color="primary"><DeviceHub /></IconButton>
                                            </h6>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell><h6 style={{ margin: 0, paddingBottom: 10, color: "#00ACC1" }}>Current Queue:</h6></TableCell>
                                        <TableCell colSpan={2}><h6 style={{ margin: 0, paddingBottom: 10 }}>{initialValues.queueName}</h6></TableCell>
                                    </TableRow>
                                </Table>
                            </div>
                            <p><br /></p>
                            <form>
                                <Table>
                                    <Heading heading1="Product" heading2="Fulfillment" />
                                    <TableRow>
                                        <TableCell style={{ verticalAlign: "top", width: "50%" }}>
                                            <ProductSection classes={classes} queueName={initialValues.queueName} acl={acl}/>
                                        </TableCell>
                                        <TableCell style={{ verticalAlign: "top", width: "50%" }}>
                                            <FormSection>
                                                <FulFillmentSection classes={classes} acl={acl}/>
                                            </FormSection>
                                        </TableCell>
                                    </TableRow>
                                    <Heading heading1="Recipient" heading2="Billing" />
                                    <TableRow>
                                        <TableCell style={{ verticalAlign: "top", width: "50%" }}>
                                            <FormSection name="recipientDetails">
                                                <AddressSection classes={classes} acl={acl}/>
                                            </FormSection>
                                        </TableCell>
                                        <TableCell style={{ verticalAlign: "top", width: "50%" }}>
                                            <FormSection name="buyerDetails">
                                                <AddressSection classes={classes} acl={acl}/>
                                                <BillingSection classes={classes} queueName={initialValues.queueName} acl={acl}/>
                                            </FormSection>
                                        </TableCell>
                                    </TableRow>
                                </Table>
                                {
                                    (initialValues.queueName !== 'ORDER_CREATED') ?
                                        <div>
                                            {
                                                (showError !== '') ?
                                                    <div style={style.error}><h4>{showError}</h4></div>
                                                    : null
                                            }
                                            {
                                                (this.state.orderSubmitInProgress === true) ?
                                                    <div className={classes.rightAlign}>
                                                        <CircularProgress size={40}
                                                            style={{ color: "rgb(202, 173, 89)" }}
                                                        />
                                                    </div>
                                                    :
                                                    <div className={classes.rightAlign}>
                                                        <Button type="submit" color="info" onClick={(event) => this.doSubmit(event)}>Save Order</Button>
                                                    </div>
                                            }
                                        </div>
                                        : null
                                }
                            </form>

                            <div id="workflow">
                                <h3 style={{ color: "#00ACC1", textTransform: "uppercase" }}>ORDER WORKFLOW</h3>
                                <label>(refreshes every 3 seconds)</label>
                                <WorkFlowUI workflowId={initialValues.workflowId} queueName={initialValues.queueName} />
                            </div>
                        </div>
                        :
                        <p>Unable to fetch order details. Please try after some time</p>
                }
            </div>
        )
    }
}

OrderForm = reduxForm({
    form: 'OrderForm',
    validate
})(OrderForm);

OrderForm.propTypes = {
    classes: PropTypes.object.isRequired
};

export default OrderForm;

function Heading(props) {
    return (
        <TableRow style={{ color: "#00ACC1", textTransform: "uppercase" }}>
            <TableCell>
                {props.heading1}
            </TableCell>
            <TableCell>
                {props.heading2}
            </TableCell>
        </TableRow>
    )
}

function ProductSection(props) {
    const { classes, acl } = props;
    const render_type = (props.queueName === 'VALIDATION_FAILED' || props.queueName === 'PAYMENT_FAILED' || props.queueName === 'FULFILLMENT_FAILED') ? "text" : "label";
    return (
        <div className={classes.table}>
            <div className={classes.tableRow}>
                <div className={`${classes.tableCell} ${classes.centerAlign}`} style={{ width: "30%", padding: 10 }}>
                    <Field acl={acl} component={renderField} render_type="string" classes={classes} name="deliveryDetails.productName"></Field><br />
                    <Field acl={acl} component={renderField} render_type="image" width="200" name="deliveryDetails.productImage"></Field>
                    <Field acl={acl} component={renderField} render_type="string" classes={classes} name="deliveryDetails.productName"></Field><br />
                    <label>(SKU: <Field acl={acl} component={renderField} render_type="string" classes={classes} name="deliveryDetails.productId"></Field>)</label>
                </div>
                <div className={classes.tableCell} style={{ width: "70%" }}>
                    <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Occasion:" name="deliveryDetails.occasion"></Field>
                    <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Gift Message:" name="deliveryDetails.giftMessage"></Field>
                    <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Quantity:" name="deliveryDetails.quantity"></Field><br />
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="Regular Price:" valueType="CUR" name="orderAmount.retailProductAmount"></Field>
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="Discounted Price (-):" valueType="CUR" name="orderAmount.discountAmount"></Field>
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="Shipping Fee:" valueType="CUR" name="orderAmount.shippingFeeAmount"></Field>
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="Tax Amount:" valueType="CUR" name="orderAmount.taxAmount"></Field>
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="OrderTotal:" valueType="CUR" name="orderAmount.orderTotal"></Field>
                </div>
            </div>
        </div>
    );
}
ProductSection.propTypes = {
    classes: PropTypes.object.isRequired
};

function AddressSection(props) {
    const { classes, acl } = props;

    return (
        <div>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="First Name:" name="firstName"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Last Name:" name="lastName"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Email Address:" name="emailAddress"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Mobile Phone Number:" name="phoneNumberMobile"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Home Phone Number:" name="phoneNumberHome"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Address Line 1:" name="address1"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Address Line 2:" name="address2"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="City:" name="city"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="State:" name="state"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Country:" name="countryCode"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Postal Code:" name="postalCode"></Field>
        </div>
    );
}
AddressSection.propTypes = {
    classes: PropTypes.object.isRequired
};

const cardTypes = ["MasterCard", "American Express", "Visa"];

function BillingSection(props) {
    const { classes, acl } = props;
    const render_type = (props.queueName === 'FRAUD_FAILED') ? "text" : "label";
    return (
        <div className={classes.table}>
            <div className={classes.tableRow}>
                <div className={classes.tableCell}>
                    <Field acl={acl} component={renderField} render_type={(props.queueName === 'FRAUD_FAILED') ? "selectfield" : "label"} classes={classes} label="Credit Card Type:" name="paymentDetails.creditCardType" options={cardTypes} />
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} maxlength="16" label="Credit Card Number:" mask name="paymentDetails.creditCardNumber"></Field>
                    {(render_type !== 'label') ? <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="CVV:" name="paymentDetails.creditCardCVV"></Field> : null}
                    <Field acl={acl} component={renderField} render_type={render_type} classes={classes} label="Expiry Date (mm/yyyy):" name="paymentDetails.creditCardExpiry"></Field>
                </div>
            </div>
        </div>
    );
}
BillingSection.propTypes = {
    classes: PropTypes.object.isRequired
};

function FulFillmentSection(props) {
    const { classes, acl } = props;
    return (
        <div>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Executing Member:" name="deliveryDetails.memberName"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Member ID:" name="deliveryDetails.memberId"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Order Creation Date:" name="orderCreationDate"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Delivery Date:" name="deliveryDetails.deliveryDate"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Delivery Service:" name="deliveryDetails.deliveryService"></Field>
            <Field acl={acl} component={renderField} render_type="label" classes={classes} label="Delivery Mode:" name="deliveryDetails.fulfillmentChannel"></Field>
        </div>
    );
}
FulFillmentSection.propTypes = {
    classes: PropTypes.object.isRequired
};

class WorkFlowUI extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            url: services("workflow", props.workflowId),
            counter: 0
        }
    }

    componentDidMount() {
        if (this.props.queueName !== 'COMPLETED') {
            setInterval(() => {
                this.setState({ counter: this.state.counter + 1, url: services("workflow", this.props.workflowId) + '&counter=' + this.state.counter });
            }, 3000)
        }
    }

    render() {
        return (
            <div className="iFrameContainer">
                <div class="iFrameOverlay" />
                <iframe
                    title="workflow"
                    className="netFlixIframe"
                    scrolling="no"
                    src={this.state.url}
                />
                <style jsx>
                    {`
                        .netFlixIframe, .iFrameOverlay {
                            height: 2300px;
                            width: 1000px;
                            border: none;
                            overflow:hidden;
                        }                       
                        .iFrameOverlay {
                            background-color:transparent;
                            position:absolute;
                            top:0;
                            left:0;
                            z-index:999;
                        }
                        .iFrameContainer {
                            position: relative;
                        }
                    `}
                </style>

            </div>


        )
    }
}
