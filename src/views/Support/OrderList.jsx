import React from "react";
import PropTypes from "prop-types";

import { TextField, IconButton, withStyles, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel, CircularProgress, Chip, Avatar } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { PaginationTable } from 'components';

import { successColor, warningColor, dangerColor, infoColor, roseColor } from "assets/jss/material-dashboard-react.jsx";
import orderStyle from "assets/jss/material-dashboard-react/orderStyle.jsx";
import OrderForm from './OrderForm.jsx';
import { getOrderDetailsById, getOrdersByQueueName, updateOrder } from 'library/actions/order.jsx';

const queueNames = [
    {
        "name": "ORDER_CREATED",
        "color": successColor
    },
    {
        "name": "VALIDATION_FAILED",
        "color": warningColor
    },
    {
        "name": "FRAUD_FAILED",
        "color": roseColor
    },
    {
        "name": "PAYMENT_FAILED",
        "color": dangerColor
    },
    {
        "name": "FULFILLMENT_FAILED",
        "color": infoColor
    }
];

const columnList = [
    {
        id: "id",
        numeric: true,
        label: "Order ID"
    },
    {
        id: "workflowId",
        numeric: false,
        label: "Workflow ID"
    },
    {
        id: "queueName",
        numeric: false,
        label: "Status"
    },
    {
        id: "orderCreationDate",
        numeric: false,
        label: "Created on"
    }
]

class OrderList extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.initialState();
    }

    initialState(override) {

        return {
            showOrderList: false,
            showOrderDetails: false,
            selectedQueue: '',
            orderList: [],
            selectedOrder: '',
            orderData: '',
            showMessage: '',
            showError: '',
            ...override
        };
    }

    selectOrderHandler = event => {
        this.setState({ searchByOrderID: event.target.value });
    }

    searchOrderHandler = event => {
        if (event.target.name === 'search_by_order' || event.target.id === 'search_by_order' || event.key === 'Enter') {
            this.setState(
                this.initialState({
                    selectedOrder: this.state.searchByOrderID
                }),
                this.fetchOrderHandler(this.state.selectedOrder)
            )
        }
    }

    searchOrderQueueHandler = event => {
        if (typeof event.target.value === 'string' && event.target.value !== '') {
            this.setState(
                this.initialState(
                    {
                        selectedQueue: event.target.value
                    }
                ),
                this.fetchOrderList
            )
        }
    }

    fetchOrderHandler = order => {
        this.setState({ selectedOrder: order },

            () => {
                getOrderDetailsById(this.state.selectedOrder)

                    .then((response) => {
                        if (response.status === 200) {
                            response.json().then((orderData) => {
                                this.setState(
                                    {
                                        showOrderDetails: true,
                                        orderData: orderData,
                                        showMessage: ''
                                    }
                                );
                                document.getElementById("orderdata").scrollIntoView();
                            });
                        } else {
                            this.setState(
                                {
                                    showOrderDetails: true,
                                    showMessage: 'Unable to fetch order details. Please try after sometime.'
                                }
                            );
                        }
                    })
                    .catch((err) => {
                        this.setState(
                            {
                                showOrderDetails: true,
                                showMessage: 'Unable to fetch order details due to an unexpected error. Please try after sometime.'
                            }
                        );
                    });
            }
        );
    }

    fetchOrderList() {

        getOrdersByQueueName(this.state.selectedQueue)

            .then((response) => {
                if (response.status === 200) {
                    response.json().then((list) => {
                        this.setState(
                            { showOrderList: true, orderList: list, showMessage: '' }
                        );
                    });
                } else {
                    this.setState(
                        {
                            showOrderList: true,
                            showMessage: 'Unable to fetch queued orders. Please try after sometime.'
                        }
                    );
                }
            }, (reject) => {
                this.setState(
                    {
                        showOrderList: true,
                        showMessage: 'Unable to fetch queued orders due to an unexpected error. Please try after sometime.'
                    }
                );
            });
    }

    saveOrder(values) {

        return updateOrder(values)
            .then((response) => {
                if (response.status === 200) {
                    const container = document.querySelector('#mainPanel');
                    container.scrollTop = 0;

                    this.setState(
                        { showMessage: 'Updated order #' + this.state.selectedOrder + ' details successfully.' }
                    );

                } else {
                    this.setState(
                        { showError: 'Unable to update order #' + this.state.selectedOrder + ' details.' }
                    );
                }

            }, (reject) => {
                this.setState(
                    { showError: 'Unable to update order #' + this.state.selectedOrder + ' details.' }
                );
            });
    }  

    render() {
        const { classes } = this.props;
        const acl = { default : 'FULL_ACCESS' };
        return (
            <div>
                <div className={classes.table}>
                    <div className={classes.tableRow}>
                        <div className={classes.tableCell} style={{ width: "67%" }}>
                            <FormControl component="fieldset" required>
                                <FormLabel component="legend">Choose Order Status:</FormLabel>
                                <RadioGroup
                                    name="orderQueue"
                                    value={this.state.selectedQueue}
                                    onChange={this.searchOrderQueueHandler}
                                    style={{ display: "inline" }}
                                >
                                    {
                                        queueNames.map(
                                            (object) => {
                                                return (
                                                    <FormControlLabel
                                                        value={object.name}
                                                        control={<Radio style={{ color: object.color }} />}
                                                        label={object.name}
                                                    />
                                                )
                                            }
                                        )
                                    }
                                </RadioGroup>
                            </FormControl>
                        </div>
                        <div className={`${classes.tableCell} ${classes.rightAlign}`} style={{ width: "20%", paddingTop: 15 }}>
                            <TextField label="Search by Order ID:" onInput={this.selectOrderHandler} onKeyPress={this.searchOrderHandler}></TextField>
                            <IconButton color="default" onClick={this.searchOrderHandler} name="search_by_order">
                                <Search id="search_by_order" />
                            </IconButton>
                        </div>
                    </div>
                </div>
                <div className={classes.centerAlign}>
                    {
                        (this.state.selectedQueue !== '') ?
                            (this.state.showOrderList === false) ?
                                <div className={classes.marginTop}>
                                    <CircularProgress
                                        size={60}
                                    />
                                </div>
                                :
                                (this.state.orderList.length === 0) ?
                                    (this.state.showMessage !== '') ?

                                        <Chip
                                            avatar={<Avatar></Avatar>}
                                            label={this.state.showMessage}
                                            className={classes.chip}
                                        />

                                        :
                                        <Chip
                                            avatar={<Avatar></Avatar>}
                                            label={`There are no orders found in ${this.state.selectedQueue} queue.`}
                                            className={classes.chip}
                                        />

                                    :
                                    <PaginationTable
                                        columnData={columnList}
                                        bodyData={this.state.orderList}
                                        onhandleClickCallback={this.fetchOrderHandler}
                                        classes={classes}
                                        keyColumn="id"
                                    />
                            : null
                    }
                </div>
                <div id="orderdata" className={classes.centerAlign}>
                    {
                        (this.state.selectedOrder !== '') ?
                            (
                                (this.state.showOrderDetails === false) ?
                                    <div className={classes.marginTop}>
                                        <CircularProgress
                                            className={classes.refreshLoading}
                                            size={60}
                                        />
                                    </div>
                                    :
                                    (this.state.showMessage !== '') ?
                                        (<Chip
                                            avatar={<Avatar></Avatar>}
                                            label={this.state.showMessage}
                                            className={classes.chip}
                                        />
                                        )
                                        :
                                        (this.state.showError !== '') ?
                                            <OrderForm acl={acl} initialValues={this.state.orderData} classes={classes} onSubmit={() => this.saveOrder()} showError={this.state.showError} />
                                            :
                                            <OrderForm acl={acl} initialValues={this.state.orderData} classes={classes} onSubmit={() => this.saveOrder()} />
                            )
                            :
                            null
                    }
                </div>
            </div>
        );
    }
}

OrderList.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(orderStyle)(OrderList);