const validateUserDetails = (userData = {}) => {
    let errors = {};

    const requiredFields = [
        'firstName',
        'lastName',
        'emailAddress',
        'address1',
        'city',
        'state',
        'countryCode',
        'postalCode',
        'phoneNumberHome'
    ];
    requiredFields.forEach(field => {
        if (!userData[field]) {
            errors[field] = "Please enter information";
        }
    });

    if (
        userData.emailAddress &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(userData.emailAddress)
    ) {
        errors.emailAddress = "Invalid email address";
    }

    return errors;

}

const validateCardDetails = (cardData = {}) => {
    let errors = {};
    
    const requiredFields = [
        "creditCardType",
        "creditCardNumber",
        "creditCardCVV",
        "creditCardExpiry"
    ];

    requiredFields.forEach(field => {
        if (!cardData[field]) {
            errors[field] = "Please enter information";
        }
    });

    if (
        cardData.creditCardNumber &&
        !/^[0-9]{16}$/i.test(cardData.creditCardNumber)
    ) {
        errors.creditCardNumber = "Invalid Credit Card Number";
    }

    if (
        cardData.creditCardExpiry &&
        !/^[0-9]{2}\/[0-9]{4}$/i.test(cardData.creditCardExpiry)
    ) {
        errors.creditCardExpiry = "Invalid Credit Card Expiry";
    }

    return errors;

}


const validateOrderAmountDetails = (amountData = {}) => {
    let errors = {};

    const requiredFields = [
        "orderTotal",
        "retailProductAmount",
        "saleProductAmount",
        "discountAmount",
        "shippingFeeAmount",
        "taxAmount"
    ];

    requiredFields.forEach(field => {
        if (!amountData[field]) {
            errors[field] = "Please enter information";
        }
    });

    return errors;
}

const validate = values => {
    
    const errors = {};
    if(values.queueName === 'VALIDATION_FAILED' || values.queueName === 'PAYMENT_FAILED' || values.queueName === 'FULFILLMENT_FAILED') {
        errors.orderAmount = validateOrderAmountDetails(values.orderAmount);
        
    }else if(values.queueName === 'FRAUD_FAILED') {
        errors.buyerDetails = {};
        errors.buyerDetails.paymentDetails = validateCardDetails(values.buyerDetails.paymentDetails);
    }
    
    errors.recipientDetails = validateUserDetails(values.recipientDetails);

    errors.deliveryDetails = {};
    if (!values.deliveryDetails.occasion) {
        errors.deliveryDetails.occasion = "Please enter occasion";
    }

    return errors;
};

export default validate;