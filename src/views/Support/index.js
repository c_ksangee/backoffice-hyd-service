import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { withStyles, Paper, Tabs, Tab, Typography } from '@material-ui/core';
import OrderList from './OrderList.jsx';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3, minHeight: 448, backgroundColor: "#fff" }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
  },
});

class Support extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div>
        <Paper className={classes.root}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"            
          >                        
            <Tab label="Order" style={{ fontWeight: 600 }}/>
            <Tab label="Customer" style={{ fontWeight: 600 }}/>
            <Tab label="Promotion" style={{ fontWeight: 600 }}/>
            <Tab label="Subscription" style={{ fontWeight: 600 }}/>
          </Tabs>
        </Paper>
        <br/>
        {value === 0 && <TabContainer><OrderList /></TabContainer>}
        {value === 1 && <TabContainer>This tab is under construction</TabContainer>}        
        {value === 2 && <TabContainer>This tab is under construction</TabContainer>}
        {value === 3 && <TabContainer>This tab is under construction</TabContainer>}
      </div>
    );
  }
}

Support = reduxForm({
  form: 'Support'
})(Support);


Support.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Support);
